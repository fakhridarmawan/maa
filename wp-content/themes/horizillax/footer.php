<?php global $smof_plugin_data; ?>
</div><!-- .wrapall -->
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer-info">
			<div class="container">
					<div class="site-info">

					
           				<?php  if(isset($smof_plugin_data['copyright_info'])):?>
            				<?php  echo $smof_plugin_data['copyright_info'];?>
            			<?php else: ?>

							<?php do_action( 'productivethemes_credits' ); ?>
							<a href="<?php echo esc_url( __( 'http://wordpress.org/', 'productivethemes' ) ); ?>" title="<?php esc_attr_e( 'Semantic Personal Publishing Platform', 'productivethemes' ); ?>"><?php printf( __( 'Proudly powered by %s', 'productivethemes' ), 'WordPress' ); ?></a>
						<?php endif; ?>
            		</div><!-- .site-info -->
			</div><!-- .wrapper -->
		</div><!-- .footer-info -->
        </footer><!-- #colophon -->
            			        
	<?php wp_footer(); ?>
	
</body>
</html>