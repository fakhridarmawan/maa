<?php get_header(); ?>
<div class="container">
	<div class="row">
				<div class="page-title">
				<h1><?php _e( 'Not found', 'productivethemes' ); ?></h1>
				</div>
						<h2><?php _e( 'This is somewhat embarrassing, isn&rsquo;t it?', 'productivethemes' ); ?></h2>
						<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'productivethemes' ); ?></p>
						<?php get_search_form(); ?>
	</div>	<!--.row -->	
</div> <!--.container -->	
<?php get_footer(); ?>