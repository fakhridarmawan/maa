<?php

add_action('init','of_options');

if (!function_exists('of_options'))
{
	function of_options()
	{
		//Access the WordPress Categories via an Array
		$of_categories 		= array();  
		$of_categories_obj 	= get_categories('hide_empty=0');
		foreach ($of_categories_obj as $of_cat) {
		    $of_categories[$of_cat->cat_ID] = $of_cat->cat_name;}
		$categories_tmp 	= array_unshift($of_categories, "Select a category:");    
	       
		//Access the WordPress Pages via an Array
		$of_pages 			= array();
		$of_pages_obj 		= get_pages('sort_column=post_parent,menu_order');    
		foreach ($of_pages_obj as $of_page) {
		    $of_pages[$of_page->ID] = $of_page->post_name; }
		$of_pages_tmp 		= array_unshift($of_pages, "Select a page:");       



		//Background Images Reader
		$bg_images_path = get_stylesheet_directory(). '/images/bg/'; // change this to where you store your bg images
		$bg_images_url = get_template_directory_uri().'/images/bg/'; // change this to where you store your bg images
		$bg_images = array();
		
		if ( is_dir($bg_images_path) ) {
		    if ($bg_images_dir = opendir($bg_images_path) ) { 
		        while ( ($bg_images_file = readdir($bg_images_dir)) !== false ) {
		            if(stristr($bg_images_file, ".png") !== false || stristr($bg_images_file, ".jpg") !== false) {
		            	natsort($bg_images); //Sorts the array into a natural order
		                $bg_images[] = $bg_images_url . $bg_images_file;
		            }
		        }    
		    }
		}
		

		/*-----------------------------------------------------------------------------------*/
		/* TO DO: Add options/functions that use these */
		/*-----------------------------------------------------------------------------------*/
		
		//More Options
		$uploads_arr 		= wp_upload_dir();
		$all_uploads_path 	= $uploads_arr['path'];
		$all_uploads 		= get_option('of_uploads');
		$other_entries 		= array("Select a number:","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19");
		$body_repeat 		= array("no-repeat","repeat-x","repeat-y","repeat");
		$body_pos 			= array("top left","top center","top right","center left","center center","center right","bottom left","bottom center","bottom right");
		
		// Image Alignment radio box
		$of_options_thumb_align = array("alignleft" => "Left","alignright" => "Right","aligncenter" => "Center"); 
		
		// Image Links to Options
		$of_options_image_link_to = array("image" => "The Image","post" => "The Post"); 


/*-----------------------------------------------------------------------------------*/
/* The Options Array */
/*-----------------------------------------------------------------------------------*/

// Set the Options Array
global $of_options;
$of_options = array();

/* General Settings */

$of_options[] = array( 	"name" 		=> "General Settings",
						"type" 		=> "heading"
				);
					
$of_options[] = array( 	"name" 		=> "Thanks!",
						"desc" 		=> "",
						"id" 		=> "introduction",
						"std" 		=> "<p style=\"margin: 0 0 10px;\">These are settings applied to the entire website.</p>",
						"icon" 		=> true,
						"type" 		=> "info"
				);


	
$of_options[] = array( 	"name" 		=> "Enable Responsive Design",
						"desc" 		=> "This adjusts the layout of your website depending on the screen size/device.",
						"id" 		=> "responsive",
						"std" 		=> 1,
						"folds"		=> 1,
						"type" 		=> "switch"
				);

$of_options[] = array( 	"name" 		=> "Rounded Corners",
						"desc" 		=> "Adds rounded corners",
						"id" 		=> "borderradius",
						"std" 		=> 1,
						"folds"		=> 1,
						"type" 		=> "switch"
				);


$of_options[] = array( 	"name" 		=> "Maximum Page width",
						"desc" 		=> "This will be the maximum width of the page. The width will automatically scaledown on lower resolutions",
						"id" 		=> "max_width",
						"std" 		=> "1000",
						"type" 		=> "text"
				);	

$of_options[] = array( 	"name" 		=> "Favicon Upload",
						"desc" 		=> "Upload your 16x16 favicon here",
						"id" 		=> "favicon",
						"std" 		=> get_template_directory_uri()."/images/favicon.png",
						"mod"		=> "min",
						"type" 		=> "media"
				);
				

$of_options[] = array( 	"name" 		=> "Background Images",
						"desc" 		=> "Select a background pattern for the page body.",
						"id" 		=> "convax_body_bg",
						"type" 		=> "tiles",
						"std" 		=> get_template_directory_uri()."/images/bg/bgnone.png",
						"options" 	=> $bg_images,
				);
				


$of_options[] = array( 	"name" 		=> "Tracking Code",
						"desc" 		=> "Please enter in your google analytics tracking code here. Remember to include the entire script from google, if you just enter your tracking ID it won't work.",
						"id" 		=> "google_analytics",
						"std" 		=> "",
						"type" 		=> "textarea"
				);
				
				
$of_options[] = array( 	"name" 		=> "Copyright Info",
						"desc" 		=> "Copyright info to be displayed on the footer",
						"id" 		=> "copyright_info",
						"std" 		=> "&copy; All Rights Reserved.",
						"type" 		=> "text"
				);	
				
/* Styling Options */				
				
$of_options[] = array( 	"name" 		=> "General Color Settings",
						"type" 		=> "heading"
				);
				
$assets_url =  ADMIN_DIR . 'assets/images/';
$of_options[] = array( 	"name" 		=> "Theme Color",
						"desc" 		=> "Choose your color scheme",
						"id" 		=> "colorstyles",
						"std" 		=> "blue.css",
						"type" 		=> "images",
						"options" 	=> array(
											'grey.css' 		=> $assets_url . 'grey.png',
											'blue.css' 	=> $assets_url . 'blue.png',
											'orange.css' 	=> $assets_url . 'orange.png',
											'green.css' 	=> $assets_url . 'green.png'
										)
				);				
				

$of_options[] = array( 	"name" 		=> "Link color",
						"desc" 		=> "Pick a color for the link.",
						"id" 		=> "link_color",
						"type" 		=> "color"
				);


$of_options[] = array( 	"name" 		=> "Link hover color",
						"desc" 		=> "Pick a color for the link.",
						"id" 		=> "link_hover_color",
						"type" 		=> "color"
				);


$of_options[] = array( 	"name" 		=> "Body background color",
						"desc" 		=> "Pick a background color for the theme.",
						"id" 		=> "body_background",
						"type" 		=> "color"
				);


$of_options[] = array( 	"name" 		=> "Input field text color",
						"desc" 		=> "Pick the color for the text in the input field.",
						"id" 		=> "input_color",
						"type" 		=> "color"
				);

$of_options[] = array( 	"name" 		=> "Input field background color",
						"desc" 		=> "Pick the background for the text field.",
						"id" 		=> "input_bg_color",
						"type" 		=> "color"
				);


$of_options[] = array( 	"name" 		=> "Button Color",
						"desc" 		=> "Color of the buttons",
						"id" 		=> "button_color",
						"type" 		=> "color"
				);

$of_options[] = array( 	"name" 		=> "Button Text Color",
						"desc" 		=> "Color of the buttons",
						"id" 		=> "button_text_color",
						"type" 		=> "color"
				);


$of_options[] = array( 	"name" 		=> "Button Color on mouse over",
						"desc" 		=> "Color of the buttons on mouse over ",
						"id" 		=> "button_color_hover",
						"type" 		=> "color"
				);

$of_options[] = array( 	"name" 		=> "Button Text Color on mouse over",
						"desc" 		=> "Color of the buttons on mouse over",
						"id" 		=> "button_text_color_hover",
						"type" 		=> "color"
				);								

				
$of_options[] = array( 	"name" 		=> "Custom CSS",
						"desc" 		=> "Quickly add some CSS to your theme by adding it to this block.",
						"id" 		=> "custom_css",
						"std" 		=> "",
						"type" 		=> "textarea"
				);
						
/* Fonts */
$of_options[] = array( 	"name" 		=> "Fonts",
						"type" 		=> "heading"
				);


$of_options[] = array( 	"name" 		=> "Body font",
						"id" 		=> "body_font",
						"std" 		=> array('size' => '13px','height'=>'25px','face' => 'arial','style' => 'normal','color' => ''),
						"type" 		=> "typography"
				);  


$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Google font - this overrides the above font with a google font",
						"id" 		=> "body_google_font",
						"std" 		=> "OpenSans",
						"type" 		=> "select_google_font",
						"preview" 	=> array(
										"text" => "This is my preview text!", 
										"size" => "15px" 
						),
						"options" 	=> array(
										"none" => "Select a font",
										"Lato" => "Lato",
										"Montserrat" => "Montserrat",
										"Open Sans" => "OpenSans",
										"Raleway" => "Raleway"
						)
				);


$of_options[] = array( 	"name" 		=> "Page Title  font",
						"id" 		=> "title_font",
						"std" 		=> array('size' => '32px','height'=>'35px','face' => 'arial','style' => 'normal','color' => ''),
						"type" 		=> "typography"
				);  

$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Google font - this overrides the above font with a google font",
						"id" 		=> "title_google_font",
						"std" 		=> "Montserrat",
						"type" 		=> "select_google_font",
						"preview" 	=> array(
										"text" => "This is my preview text!", 
										"size" => "30px" 
						),
						"options" 	=> array(
										"none" => "Select a font",
										"Lato" => "Lato",
										"Montserrat" => "Montserrat",
										"Open Sans" => "OpenSans",
										"Raleway" => "Raleway"
						)
				);

$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Capitalize",
						"id" 		=> "title_capitalize",
						"std" 		=> 1,
						"type" 		=> "checkbox"
				);


$of_options[] = array( 	"name" 		=> "Page Sub Title  font",
						"id" 		=> "subtitle_font",
						"std" 		=> array('size' => '15px','height'=>'20px','face' => 'arial','style' => 'normal','color' => ''),
						"type" 		=> "typography"
				);  

$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Google font - this overrides the above font with a google font",
						"id" 		=> "subtitle_google_font",
						"std" 		=> "Montserrat",
						"type" 		=> "select_google_font",
						"preview" 	=> array(
										"text" => "This is my preview text!", 
										"size" => "30px" 
						),
						"options" 	=> array(
										"none" => "Select a font",
										"Lato" => "Lato",
										"Montserrat" => "Montserrat",
										"Open Sans" => "OpenSans",
										"Raleway" => "Raleway"
						)
				);

$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Capitalize",
						"id" 		=> "subtitle_capitalize",
						"std" 		=> 1,
						"type" 		=> "checkbox"
				);


$of_options[] = array( 	"name" 		=> "Menu font",
						"id" 		=> "menu_font",
						"std" 		=> array('size' => '13px','face' => 'arial','style' => 'normal','color' => '','height' => '20px'),
						"type" 		=> "typography"
				);  

$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Google font - this overrides the above font with a google font",
						"id" 		=> "menu_google_font",
						"std" 		=> "Montserrat",
						"type" 		=> "select_google_font",
						"preview" 	=> array(
										"text" => "This is my preview text!", 
										"size" => "15px" 
						),
						"options" 	=> array(
										"none" => "Select a font",
										"Lato" => "Lato",
										"Montserrat" => "Montserrat",
										"Open Sans" => "OpenSans",
										"Raleway" => "Raleway"
						)
				);

$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Capitalize",
						"id" 		=> "menu_capitalize",
						"std" 		=> 1,
						"type" 		=> "checkbox"
				);


$of_options[] = array( 	"name" 		=> "Widget Title font",
						"id" 		=> "widget_font",
						"std" 		=> array('size' => '13px','face' => 'arial','style' => 'normal','color' => '','height' => '20px'),
						"type" 		=> "typography"
				);  

$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Google font - this overrides the above font with a google font",
						"id" 		=> "widget_google_font",
						"std" 		=> "Montserrat",
						"type" 		=> "select_google_font",
						"preview" 	=> array(
										"text" => "This is my preview text!", 
										"size" => "30px" 
						),
						"options" 	=> array(
										"none" => "Select a font",
										"Lato" => "Lato",
										"Montserrat" => "Montserrat",
										"Open Sans" => "OpenSans",
										"Raleway" => "Raleway"
						)
				);

$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Capitalize",
						"id" 		=> "widget_capitalize",
						"std" 		=> 1,
						"type" 		=> "checkbox"
				);
				

$of_options[] = array( 	"name" 		=> "Headings",
						"id" 		=> "headings_font",
						"std" 		=> array('size' => '26px','face' => 'arial','style' => 'bold','color' => '','height'=>'29px'),
						"type" 		=> "typography"
				);  

$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Google font - this overrides the above font with a google font",
						"id" 		=> "headings_google_font",
						"std" 		=> "Raleway",
						"type" 		=> "select_google_font",
						"preview" 	=> array(
										"text" => "This is my preview text!", 
										"size" => "30px" 
						),
						"options" 	=> array(
										"none" => "Select a font",
										"Lato" => "Lato",
										"Montserrat" => "Montserrat",
										"Open Sans" => "OpenSans",
										"Raleway" => "Raleway"
						)
				);

$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Capitalize",
						"id" 		=> "blogpost_capitalize",
						"std" 		=> 0,
						"type" 		=> "checkbox"
				);


/* Preloader Settings */

$of_options[] = array( 	"name" 		=> "Preloader Settings",
						"type" 		=> "heading"
				);

$of_options[] = array( 	"name" 		=> "Show Preloader",
						"desc" 		=> "Show or hide preloader",
						"id" 		=> "show_preloader",
						"std" 		=> 1,
						"folds"		=> 1,
						"type" 		=> "switch"
				);

$of_options[] = array( 	"name" 		=> "Preloader background color",
						"desc" 		=> "Color of the preloader background ",
						"id" 		=> "preloader_bg_color",
						"type" 		=> "color"
				);


$of_options[] = array( 	"name" 		=> "Preloader bar color",
						"desc" 		=> "Color of the preloader bar ",
						"id" 		=> "preloader_bar_color",
						"type" 		=> "color"
				);

$of_options[] = array( 	"name" 		=> "Preloader progress bar color",
						"desc" 		=> "Color of the preloader progress bar ",
						"id" 		=> "preloader_progress_bar_color",
						"type" 		=> "color"
				);
				
								
$of_options[] = array( 	"name" 		=> "Preloader text color",
						"desc" 		=> "Color of the percentage text ",
						"id" 		=> "preloader_text_color",
						"type" 		=> "color"
				);
								

/* Header Settings */

$of_options[] = array( 	"name" 		=> "Header Settings",
						"type" 		=> "heading"
				);

$of_options[] = array( 	"name" 		=> "Logo Upload",
						"desc" 		=> "Upload your logo here",
						"id" 		=> "header_logo",
						"std" 		=> get_template_directory_uri()."/images/logo.png",
						"mod"		=> "min",
						"type" 		=> "media"
				);


$of_options[] = array( 	"name" 		=> "Header Background",
						"desc" 		=> "Pick a color for the header background.",
						"id" 		=> "header_bg_color",
						"type" 		=> "color"
				);		
									

$of_options[] = array( 	"name" 		=> "Menu item color",
						"desc" 		=> "Pick a color for the menu item.",
						"id" 		=> "menu_link_color",
						"type" 		=> "color"
				);	

$of_options[] = array( 	"name" 		=> "Menu item hover color",
						"desc" 		=> "Pick a mouse over color for the menu item.",
						"id" 		=> "menu_link_color_hover",
						"type" 		=> "color"
				);	


$of_options[] = array( 	"name" 		=> "Active menu item color",
						"desc" 		=> "Pick a mouse over color for the menu item.",
						"id" 		=> "active_link_color",
						"type" 		=> "color"
				);	

$of_options[] = array( 	"name" 		=> "Active menu item background",
						"desc" 		=> "Pick a color for the menu item background.",
						"id" 		=> "active_link_bg",
						"type" 		=> "color"
				);	

$of_options[] = array( 	"name" 		=> "Social Networks to show on header",
						"desc" 		=> "Twitter",
						"id" 		=> "soc_twitter",
						"std" 		=> 1,
						"type" 		=> "checkbox"
				);

$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Facebook",
						"id" 		=> "soc_facebook",
						"std" 		=> 1,
						"type" 		=> "checkbox"
				);				

$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Youtube",
						"id" 		=> "soc_youtube",
						"std" 		=> 0,
						"type" 		=> "checkbox"
				);

$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Linked In",
						"id" 		=> "soc_linkedin",
						"std" 		=> 1,
						"type" 		=> "checkbox"
				);	

$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Flickr",
						"id" 		=> "soc_flickr",
						"std" 		=> 0,
						"type" 		=> "checkbox"
				);

$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Pinterest",
						"id" 		=> "soc_pinterest",
						"std" 		=> 0,
						"type" 		=> "checkbox"
				);	
				
$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Google Plus",
						"id" 		=> "soc_googleplus",
						"std" 		=> 0,
						"type" 		=> "checkbox"
				);								

$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Dribbble",
						"id" 		=> "soc_dribbble",
						"std" 		=> 1,
						"type" 		=> "checkbox"
				);

$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Vimeo",
						"id" 		=> "soc_vimeo",
						"std" 		=> 0,
						"type" 		=> "checkbox"
				);				

$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Tumblr",
						"id" 		=> "soc_tumblr",
						"std" 		=> 0,
						"type" 		=> "checkbox"
				);	

$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Instagram",
						"id" 		=> "soc_instagram",
						"std" 		=> 0,
						"type" 		=> "checkbox"
				);					

$of_options[] = array( 	"name" 		=> "",
						"desc" 		=> "Github",
						"id" 		=> "soc_github",
						"std" 		=> 0,
						"type" 		=> "checkbox"
				);
																						
/* Parallax Elements */

$of_options[] = array( 	"name" 		=> "Parallax Elements",
						"type" 		=> "heading"
				);

$of_options[] = array( 	"name" 		=> "Parallax Elements",
						"desc" 		=> "",
						"id" 		=> "parallax_elems_introduction",
						"std" 		=> "<h1>Parallax Elements</h1><p style=\"margin: 0 0 10px;\">Here you can set the position of the elements on each screen, the mouse move speed, the vertical offset and the stack order.</p>",
						"icon" 		=> true,
						"type" 		=> "info"
				);

$of_options[] = array( 	"name" 		=> "Parallax Animation Speed",
						"desc" 		=> "Set the overall speed of the parallax movement",
						"id" 		=> "parallax_speed",
						"std" 		=> "2500",
						"min" 		=> "1",
						"step"		=> "1",
						"max" 		=> "3000",
						"type" 		=> "sliderui" 
				);

$of_options[] = array( 	"name" 		=> "Parallax Elements",
						"desc" 		=> "",
						"id" 		=> "pingu_slider",
						"std" 		=> "",
						"type" 		=> "slider"
				);


/* Parallax Settings */



$of_options[] = array( 	"name" 		=> "Parallax Template Settings",
						"type" 		=> "heading"
				);


$of_options[] = array( 	"name" 		=> "Parallax Page Settings",
						"desc" 		=> "",
						"id" 		=> "parallax_introduction",
						"std" 		=> "<h1>Parallax Page Settings</h1><p style=\"margin: 0 0 10px;\">These settings are specific to the parallax template.</p>",
						"icon" 		=> true,
						"type" 		=> "info"
				);

				
$of_options[] = array( 	"name" 		=> "Content Area Width",
						"desc" 		=> "Width of each parallax content box",
						"id" 		=> "parallax_width",
						"std" 		=> "760",
						"type" 		=> "text"
				);	


$of_options[] = array( 	"name" 		=> "Parallax animation on mouse move",
						"desc" 		=> "Parallax motion that happens when you move the mouse cursor",
						"id" 		=> "parallax_mousemove",
						"std" 		=> 1,
						"folds"		=> 1,
						"type" 		=> "switch"
				);


$of_options[] = array( 	"name" 		=> "On mouse over, bring content to front",
						"desc" 		=> "Stacks the content area above all parallax layers on mouse over.",
						"id" 		=> "content_stack",
						"std" 		=> 1,
						"folds"		=> 1,
						"type" 		=> "switch"
				);


$of_options[] = array( 	"name" 		=> "Parallax on Mousewheel",
						"desc" 		=> "Enables parallax animation while scrolling/wheeling the mouse",
						"id" 		=> "mousewheel_parallax",
						"std" 		=> 1,
						"type" 		=> "switch"
				);

$of_options[] = array( 	"name" 		=> "Parallax on Swipe",
						"desc" 		=> "Enables parallax animation on swipe, for touch enabled devices.",
						"id" 		=> "swipe_parallax",
						"std" 		=> 1,
						"type" 		=> "switch"
				);


$of_options[] = array( 	"name" 		=> "Parallax on Key Press",
						"desc" 		=> "Enables parallax animation on pressing the right/left arrows of the keyboard",
						"id" 		=> "keyboard_parallax",
						"std" 		=> 1,
						"type" 		=> "switch"
				);


$of_options[] = array( 	"name" 		=> "Text Color",
						"desc" 		=> "Pick a text color for the parallax template content.",
						"id" 		=> "parallax_text_color",
						"type" 		=> "color"
				);



$of_options[] = array( 	"name" 		=> "Link color",
						"desc" 		=> "Pick a color for the link.",
						"id" 		=> "parallax_link_color",
						"type" 		=> "color"
				);


$of_options[] = array( 	"name" 		=> "Link hover color",
						"desc" 		=> "Pick a color for the link.",
						"id" 		=> "parallax_hover_color",
						"type" 		=> "color"
				);


				
$of_options[] = array( 	"name" 		=> "Scrollbar color",
						"desc" 		=> "Color of the custom scrollbar",
						"id" 		=> "scrollbar_color",
						"type" 		=> "color"
				);
						

$of_options[] = array( 	"name" 		=> "Content Box Settings",
						"desc" 		=> "",
						"id" 		=> "content_introduction",
						"std" 		=> "<h1>Content Box Settings</h1><p style=\"margin: 0 0 10px;\">These settings are applied only for pages that have the 'Content Box' enabled.</p>",
						"icon" 		=> true,
						"type" 		=> "info"
				);


$of_options[] = array( 	"name" 		=> "Text Color",
						"desc" 		=> "Pick a color for the text within contact box.",
						"id" 		=> "contentbox_text",
						"type" 		=> "color"
				);
				
$of_options[] = array( 	"name" 		=> "Link color",
						"desc" 		=> "Pick a color for the link.",
						"id" 		=> "contentbox_link_color",
						"type" 		=> "color"
				);


$of_options[] = array( 	"name" 		=> "Link hover color",
						"desc" 		=> "Pick a color for the link.",
						"id" 		=> "contentbox_hover_color",
						"type" 		=> "color"
				);

				
$of_options[] = array( 	"name" 		=> "Page Title Color",
						"desc" 		=> "Pick a color for the page title.",
						"id" 		=> "contentbox_pagetitle",
						"type" 		=> "color"
				);

$of_options[] = array( 	"name" 		=> "Sub Title Color",
						"desc" 		=> "Pick a color for the sub title.",
						"id" 		=> "contentbox_subtitle",
						"type" 		=> "color"
				);


$of_options[] = array( 	"name" 		=> "Scrollbar Color",
						"desc" 		=> "Color of the custom scrollbar",
						"id" 		=> "scrollbar_contentbox_color",
						"type" 		=> "color"
				);


$of_options[] = array( 	"name" 		=> "Box Background Color",
						"desc" 		=> "Pick a background color for the contact box.",
						"id" 		=> "contentbox_bg",
						"type" 		=> "color"
				);






/* Blog Settings */				
				
$of_options[] = array( 	"name" 		=> "Page and Blog Settings",
						"type" 		=> "heading"
				);


$of_options[] = array( 	"name" 		=> "Text color",
						"desc" 		=> "Pick a text color for the blog.",
						"id" 		=> "blog_text_color",
						"type" 		=> "color"
				);



$of_options[] = array( 	"name" 		=> "Blog post and widget background color",
						"desc" 		=> "Pick a text color for the blog post title.",
						"id" 		=> "blog_bg_color",
						"type" 		=> "color"
				);

$of_options[] = array( 	"name" 		=> "Link color",
						"desc" 		=> "Pick a color for the link.",
						"id" 		=> "blog_link_color",
						"type" 		=> "color"
				);


$of_options[] = array( 	"name" 		=> "Link hover color",
						"desc" 		=> "Pick a color for the link.",
						"id" 		=> "blog_link_hover_color",
						"type" 		=> "color"
				);


$assets_url =  ADMIN_DIR . 'assets/images/';



/* Portfolio Settings */				
				
$of_options[] = array( 	"name" 		=> "Portfolio Settings",
						"type" 		=> "heading"
				);

	
$of_options[] = array( 	"name" 		=> "Enable Filterable Portfolio",
						"desc" 		=> "A filterable portfolio based on categories is shown",
						"id" 		=> "portfolio_filterable",
						"std" 		=> 1,
						"type" 		=> "switch"
				);


$of_options[] = array( 	"name" 		=> "Enable Pagination",
						"desc" 		=> "projects will be divided into pages (useful if you have many projects in your portfolio)",
						"id" 		=> "portfolio_pagination",
						"std" 		=> 0,
						"type" 		=> "switch"
				);


$of_options[] = array( 	"name" 		=> "Related Projects Title",
						"desc" 		=> "Title for the related projects section in the portfolio single page",
						"id" 		=> "related_projects_title",
						"std" 		=> "Related Projects",
						"type" 		=> "text"
				);



/* Social Media */

$of_options[] = array( 	"name" 		=> "Social Media",
						"type" 		=> "heading"
				);

								
$of_options[] = array( 	"name" 		=> "Twitter",
						"id" 		=> "twitter_url",
						"std" 		=> "http://www.twitter.com",
						"type" 		=> "text"
				);

$of_options[] = array( 	"name" 		=> "Facebook",
						"id" 		=> "facebook_url",
						"std" 		=> "http://www.facebook.com",
						"type" 		=> "text"
				);				

$of_options[] = array( 	"name" 		=> "Youtube",
						"id" 		=> "youtube_url",
						"std" 		=> "http://www.youtube.com",
						"type" 		=> "text"
				);	

$of_options[] = array( 	"name" 		=> "Linked In",
						"id" 		=> "linkedin_url",
						"std" 		=> "http://www.linkedin.com",
						"type" 		=> "text"
				);	

$of_options[] = array( 	"name" 		=> "Flickr",
						"id" 		=> "flickr_url",
						"std" 		=> "http://www.flickr.com",
						"type" 		=> "text"
				);		
					
$of_options[] = array( 	"name" 		=> "Pinterest",
						"id" 		=> "pinterest_url",
						"std" 		=> "http://www.pinterest.com",
						"type" 		=> "text"
				);			
				
$of_options[] = array( 	"name" 		=> "Google Plus",
						"id" 		=> "googleplus_url",
						"std" 		=> "http://plus.google.com",
						"type" 		=> "text"
				);
				
$of_options[] = array( 	"name" 		=> "Dribbble",
						"id" 		=> "dribbble_url",
						"std" 		=> "http://www.dribbble.com",
						"type" 		=> "text"
				);	
				
$of_options[] = array( 	"name" 		=> "Vimeo",
						"id" 		=> "vimeo_url",
						"std" 		=> "http://www.vimeo.com",
						"type" 		=> "text"
				);	

$of_options[] = array( 	"name" 		=> "Tumblr",
						"id" 		=> "tumblr_url",
						"std" 		=> "http://www.tumblr.com",
						"type" 		=> "text"
				);	

$of_options[] = array( 	"name" 		=> "Instagram",
						"id" 		=> "instagram_url",
						"std" 		=> "http://www.instagram.com",
						"type" 		=> "text"
				);					

$of_options[] = array( 	"name" 		=> "Github",
						"id" 		=> "github_url",
						"std" 		=> "http://www.github.com",
						"type" 		=> "text"
				);		
								
					

				
		


				
				
/* Backup Options */

$of_options[] = array( 	"name" 		=> "Backup Options",
						"type" 		=> "heading",
						"icon"		=> ADMIN_IMAGES . "icon-slider.png"
				);
				
$of_options[] = array( 	"name" 		=> "Backup and Restore Options",
						"id" 		=> "of_backup",
						"std" 		=> "",
						"type" 		=> "backup",
						"desc" 		=> 'You can use the two buttons below to backup your current options, and then restore it back at a later time. This is useful if you want to experiment on the options but would like to keep the old settings in case you need it back.',
				);
				
$of_options[] = array( 	"name" 		=> "Transfer Theme Options Data",
						"id" 		=> "of_transfer",
						"std" 		=> "",
						"type" 		=> "transfer",
						"desc" 		=> 'You can tranfer the saved options data between different installs by copying the text inside the text box. To import data from another install, replace the data in the text box with the one from another install and click "Import Options".',
				);
				
	}//End function: of_options()
}//End chack if function exists: of_options()
?>
