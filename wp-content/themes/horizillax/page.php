<?php get_header(); ?>

<div class="container">
	<div class="row">
		<div class="col-sm-9">
			<div id="primary" class="content-area equalheight">
			<div id="content" class="site-content" role="main">
				<?php while ( have_posts() ) : the_post(); ?>
				
				
									
					
					<div class="page-title"><h1><?php echo apply_filters( 'the_title', get_the_title( ) );?></h1>
					<?php if(get_field("heading_title")): ?>
                    	<h4><?php the_field("heading_title"); ?></h4>
                    <?php endif; ?><br/>
                    <?php if(get_field("sub_heading")): ?>
                    	<div class="page-desc"><?php the_field("sub_heading"); ?></div>
                     <?php endif; ?>
                     
					<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
							<?php the_post_thumbnail(); ?>
						</div><!-- .entry-thumbnail -->
					<?php endif; ?>

            
           			 </div>

					
						
						

				
				
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					
						<div class="entry-content">
							<?php the_content(); ?>
							<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'productivethemes' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
						</div><!-- .entry-content -->
						<footer class="entry-meta">
							<?php edit_post_link( __( 'Edit', 'productivethemes' ), '<span class="edit-link">', '</span>' ); ?>
						</footer><!-- .entry-meta -->
					</article><!-- #post -->
					<?php comments_template(); ?>
				<?php endwhile; ?>
            </div><!-- #content -->
        </div><!-- #primary -->
        
        
        </div> <!--.col-sm-9 -->
	</div>	<!--.row -->	
</div> <!--.container -->	        
<?php get_footer(); ?>