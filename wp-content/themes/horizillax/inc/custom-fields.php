<?php
include_once('addons/acf-repeater/acf-repeater.php');
include_once('addons/acf-flexible-content/acf-flexible-content.php');

if(function_exists("register_field_group"))
{


/*=================================================
	All Pages
==================================================*/

/* Global Page Settings */

    register_field_group(array (
        'id' => 'global-settings',
        'title' => 'Page Settings',
        'fields' => array (
        
                    		
            array (
                'key' => 'field_12284e77d3cdd',
                'label' => 'Sub Title',
                'name' => 'heading_title',
                'type' => 'text',
                'instructions' => 'The heading text on the top of this page',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_52284e77d3cde',
                'label' => 'Short Description',
                'name' => 'sub_heading',
                'type' => 'text',
                'instructions' => 'The next just under the heading',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '',
            ),
			
								
                      
        ),
        'location' => array (
            array (
                array (
                    'param' => 'page_template',
                    'operator' => '==',
                    'value' => 'default',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        
        
        'menu_order' => 0,
    ));
    
    



/* Parallax Page Settings */

    register_field_group(array (
        'id' => 'parallax-settings',
        'title' => 'Parallax Page Settings',
        'fields' => array (
        
                    		
            array (
                'key' => 'field_524010ca38943123asd',
                'label' => 'Top offset for the content area',
                'name' => 'top_offset',
                'type' => 'text',
                'instructions' => 'pixels to move the content area vertically. PS: Negative values will move it further up',
                'default_value' => '0',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '5',
            ),
            

			array (
                'key' => 'field_524120fu38893123asd',
                'label' => 'Show or Hide Contents',
                'name' => 'show_contents',
                'type' => 'radio',
                'choices' => array (
                    'Show' => 'Show',
                    'Hide' => 'Hide',
                ),
                'other_choice' => 0,
                'save_other_choice' => 0,
                'default_value' => 'Show',
                'layout' => 'horizontal',
            ),  
            
            
            array (
                'key' => 'field_524010fu38943123asd',
                'label' => 'Show/Hide "Read More" button',
                'instructions' => 'Shows "Read More" button on top of each page/content area.',
                'name' => 'show_readmore',
                'type' => 'radio',
                'choices' => array (
                    'Show' => 'Show',
                    'Hide' => 'Hide',
                ),
                'other_choice' => 0,
                'save_other_choice' => 0,
                'default_value' => '',
                'layout' => 'horizontal',
            ),
            
            array (
                'key' => 'field_524120fu38943123asd',
                'label' => 'Enable/Disable "Content Box"',
                'instructions' => 'Wraps all contents in a box',
                'name' => 'show_contentbox',
                'type' => 'radio',
                'choices' => array (
                    'Enable' => 'Enable',
                    'Disable' => 'Disable',
                ),
                'other_choice' => 0,
                'save_other_choice' => 0,
                'default_value' => 'Enable',
                'layout' => 'horizontal',
            ),     
            
            array (
                'key' => 'field_524120fu38961123asd',
                'label' => 'Enable/Disable "Page Title"',
                'instructions' => 'Show or Hide the title of the page',
                'name' => 'show_pagetitle',
                'type' => 'radio',
                'choices' => array (
                    'Show' => 'Show',
                    'Hide' => 'Hide',
                ),
                'other_choice' => 0,
                'save_other_choice' => 0,
                'default_value' => 'Show',
                'layout' => 'horizontal',
            ),                     
                                  
        ),
        'location' => array (
            array (
                array (
                    'param' => 'page_template',
                    'operator' => '==',
                    'value' => 'default',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        
        
        'menu_order' => 0,
    ));
    

/*=================================================
	Portfolio Template
==================================================*/

/* Global Page Settings */

    register_field_group(array (
        'id' => 'portfolio-global-settings',
        'title' => 'Page Settings',
        'fields' => array (

            		
            array (
                'key' => 'field_12284e77d3por',
                'label' => 'Sub Title',
                'name' => 'heading_title',
                'type' => 'text',
                'instructions' => 'The heading text on the top of this page',
                'default_value' => 'Our Work',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_52284e77d3por',
                'label' => 'Short Description',
                'name' => 'sub_heading',
                'type' => 'text',
                'instructions' => 'The next just under the heading',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '',
            ),

															
                      
        ),
        'location' => array (
            array (
                array (
                    'param' => 'page_template',
                    'operator' => '==',
                    'value' => 'template-portfolio.php',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        
        
        'menu_order' => 0,
    ));


/* Parallax Page Settings */

    register_field_group(array (
        'id' => 'portfolio-parallax-settings',
        'title' => 'Parallax Page Settings',
        'fields' => array (
        
                    		
            array (
                'key' => 'field_524010ca38943123por',
                'label' => 'Top offset for the content area',
                'name' => 'top_offset',
                'type' => 'text',
                'instructions' => 'pixels to move the content area vertically. PS: Negative values will move it further up',
                'default_value' => '0',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '5',
            ),

			array (
                'key' => 'field_524120fu38893123por',
                'label' => 'Show or Hide Contents',
                'name' => 'show_contents',
                'type' => 'radio',
                'choices' => array (
                    'Show' => 'Show',
                    'Hide' => 'Hide',
                ),
                'other_choice' => 0,
                'save_other_choice' => 0,
                'default_value' => 'Show',
                'layout' => 'horizontal',
            ),  

            array (
                'key' => 'field_524010fu38943123por',
                'label' => 'Show/Hide "Read More" button',
                'instructions' => 'Shows "Read More" button on top of each page/content area.',
                'name' => 'show_readmore',
                'type' => 'radio',
                'choices' => array (
                    'Show' => 'Show',
                    'Hide' => 'Hide',
                ),
                'other_choice' => 0,
                'save_other_choice' => 0,
                'default_value' => '',
                'layout' => 'horizontal',
            ),
            
            array (
                'key' => 'field_524120fu38943123por',
                'label' => 'Enable/Disable "Content Box"',
                'instructions' => 'Wraps all contents in a box',
                'name' => 'show_contentbox',
                'type' => 'radio',
                'choices' => array (
                    'Enable' => 'Enable',
                    'Disable' => 'Disable',
                ),
                'other_choice' => 0,
                'save_other_choice' => 0,
                'default_value' => 'Enable',
                'layout' => 'horizontal',
            ),     
            
            array (
                'key' => 'field_524120fu38961123por',
                'label' => 'Enable/Disable "Page Title"',
                'instructions' => 'Show or Hide the title of the page',
                'name' => 'show_pagetitle',
                'type' => 'radio',
                'choices' => array (
                    'Show' => 'Show',
                    'Hide' => 'Hide',
                ),
                'other_choice' => 0,
                'save_other_choice' => 0,
                'default_value' => 'Show',
                'layout' => 'horizontal',
            ),
            
            
            array (
                'key' => 'field_556010ca38943123por',
                'label' => 'Number of projects to show',
                'name' => 'num_projects',
                'type' => 'text',
                'instructions' => 'Number of project to show in the parallax page. Ensure that the pagination is enabled under Appearance > Theme Options > Portfolio Settings',
                'default_value' => '5',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '2',
            ),   
            
            array (
                'key' => 'field_537eefbd87por',
                'label' => 'Include Only Those Categories',
                'name' => 'include_only_those_categories',
                'type' => 'text',
                'instructions' => 'Type category Slug\'s on a comma separated list. eg: video,audio',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_527eefd887por',
                'label' => 'Exclude Posts',
                'name' => 'exclude_posts',
                'type' => 'relationship',
                'instructions' => 'Click posts, and place them to the right if you want to exclude them from home-page',
                'return_format' => 'object',
                'post_type' => array (
                    0 => 'portfolio_item',
                ),
                'taxonomy' => array (
                    0 => 'all',
                ),
                'filters' => array (
                    0 => 'search',
                ),
                'result_elements' => array (
                    0 => 'post_type',
                    1 => 'post_title',
                ),
                'max' => '',
            ),
                                          
                                  
        ),
        'location' => array (
            array (
                array (
                    'param' => 'page_template',
                    'operator' => '==',
                    'value' => 'template-portfolio.php',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        
        
        'menu_order' => 0,
    ));



/*=================================================
	Parallax Template
==================================================*/

/* Global Page Settings */

    register_field_group(array (
        'id' => 'parallax-global-settings',
        'title' => 'Page Settings',
        'fields' => array (

            		
            array (
                'key' => 'field_12284e77d3par',
                'label' => 'Sub Title',
                'name' => 'heading_title',
                'type' => 'text',
                'instructions' => 'The heading text on the top of this page',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_52284e77d3par',
                'label' => 'Short Description',
                'name' => 'sub_heading',
                'type' => 'text',
                'instructions' => 'The next just under the heading',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '',
            ),

															
                      
        ),
        'location' => array (
            array (
                array (
                    'param' => 'page_template',
                    'operator' => '==',
                    'value' => 'template-parallax.php',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        
        
        'menu_order' => 0,
    ));



/* Parallax Page Settings */

    register_field_group(array (
        'id' => 'parallax-parallax-settings',
        'title' => 'Parallax Page Settings',
        'fields' => array (
        
                    		
            array (
                'key' => 'field_524010ca38943123par',
                'label' => 'Top offset for the content area',
                'name' => 'top_offset',
                'type' => 'text',
                'instructions' => 'pixels to move the content area vertically. PS: Negative values will move it further up',
                'default_value' => '0',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '5',
            ),


			array (
                'key' => 'field_524120fu38893123par',
                'label' => 'Show or Hide Contents',
                'name' => 'show_contents',
                'type' => 'radio',
                'choices' => array (
                    'Show' => 'Show',
                    'Hide' => 'Hide',
                ),
                'other_choice' => 0,
                'save_other_choice' => 0,
                'default_value' => 'Show',
                'layout' => 'horizontal',
            ),  
                        
            array (
                'key' => 'field_524120fu38943123par',
                'label' => 'Enable/Disable "Content Box"',
                'instructions' => 'Wraps all contents in a box',
                'name' => 'show_contentbox',
                'type' => 'radio',
                'choices' => array (
                    'Enable' => 'Enable',
                    'Disable' => 'Disable',
                ),
                'other_choice' => 0,
                'save_other_choice' => 0,
                'default_value' => 'Enable',
                'layout' => 'horizontal',
            ),  
            
                           
            
            array (
                'key' => 'field_524120fu38961123par',
                'label' => 'Show/Hide "Page Title"',
                'instructions' => 'Show or Hide the title of the page',
                'name' => 'show_pagetitle',
                'type' => 'radio',
                'choices' => array (
                    'Show' => 'Show',
                    'Hide' => 'Hide',
                ),
                'other_choice' => 0,
                'save_other_choice' => 0,
                'default_value' => 'Show',
                'layout' => 'horizontal',
            ),                     
                                  
        ),
        'location' => array (
            array (
                array (
                    'param' => 'page_template',
                    'operator' => '==',
                    'value' => 'template-parallax.php',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        
        
        'menu_order' => 0,
    ));







/*=================================================
	About Template
==================================================*/


    register_field_group(array (
        'id' => 'about-settings',
        'title' => 'About',
        'fields' => array (
									
            array (
                'key' => 'field_522ef34a99534',
                'label' => 'Team Members',
                'name' => 'team_members',
                'type' => 'repeater',
                'instructions' => 'Add as many members as you need from this field, click add member to add a new one.',
                'sub_fields' => array (
                    array (
                        'key' => 'field_522ef36899535',
                        'label' => 'Member Name',
                        'name' => 'member_name',
                        'type' => 'text',
                        'column_width' => '',
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'formatting' => 'html',
                        'maxlength' => '',
                    ),
					
                    array (
                        'key' => 'field_522ef36999535',
                        'label' => 'Member Designation',
                        'name' => 'member_designation',
                        'type' => 'text',
                        'column_width' => '',
                        'default_value' => 'Web Developer',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'formatting' => 'html',
                        'maxlength' => '',
                    ),
					array (
                        'key' => 'field_522ef37199536',
                        'label' => 'Member Description',
                        'name' => 'member_description',
                        'type' => 'textarea',
                        'column_width' => '',
                        'default_value' => '',
                        'placeholder' => '',
                        'maxlength' => '',
                        'formatting' => 'html',
                    ),
					
					array (
                        'key' => 'field_522df37199536',
                        'label' => 'Twitter URL',
                        'name' => 'about_twitter',
                        'type' => 'text',
                        'column_width' => '',
                        'default_value' => '',
                        'placeholder' => '',
                        'maxlength' => '',
                        'formatting' => 'html',
                    ),
					
					array (
                        'key' => 'field_522df37199537',
                        'label' => 'Facebook URL',
                        'name' => 'about_facebook',
                        'type' => 'text',
                        'column_width' => '',
                        'default_value' => '',
                        'placeholder' => '',
                        'maxlength' => '',
                        'formatting' => 'html',
                    ),
					
					array (
                        'key' => 'field_522df37199538',
                        'label' => 'LinkedIn URL',
                        'name' => 'about_linkedin',
                        'type' => 'text',
                        'column_width' => '',
                        'default_value' => '',
                        'placeholder' => '',
                        'maxlength' => '',
                        'formatting' => 'html',
                    ),
					
	                    array (
                        'key' => 'field_522ef37d99537',
                        'label' => 'Member Avatar/Image',
                        'name' => 'member_avatar',
                        'type' => 'image',
                        'column_width' => '',
                        'save_format' => 'object',
                        'preview_size' => 'thumbnail',
                        'library' => 'all',
                    ),
                ),
                'row_min' => 0,
                'row_limit' => '',
                'layout' => 'table',
                'button_label' => 'Add Member',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'page_template',
                    'operator' => '==',
                    'value' => 'template-about.php',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
                0 => 'the_content',
            ),
        ),
        'menu_order' => 0,
    ));



/* Global Page Settings */
    register_field_group(array (
        'id' => 'about-global-settings',
        'title' => 'Page Settings',
        'fields' => array (
        
                    		
            array (
                'key' => 'field_12284e77d3abo',
                'label' => 'Sub Title',
                'name' => 'heading_title',
                'type' => 'text',
                'instructions' => 'The heading text on the top of this page',
                'default_value' => 'Our Team',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '',
            ),
            array (
                'key' => 'field_52284e77d3abo',
                'label' => 'Short Description',
                'name' => 'sub_heading',
                'type' => 'text',
                'instructions' => 'The next just under the heading',
                'default_value' => 'Meet Our Team Members',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '',
            ),

			
								
                      
        ),
        'location' => array (
            array (
                array (
                    'param' => 'page_template',
                    'operator' => '==',
                    'value' => 'template-about.php',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        
        
        'menu_order' => 0,
    ));
    



/* Parallax Page Settings */

    register_field_group(array (
        'id' => 'about-parallax-settings',
        'title' => 'Parallax Page Settings',
        'fields' => array (
        
                    		
            array (
                'key' => 'field_524010ca38943123abo',
                'label' => 'Top offset for the content area',
                'name' => 'top_offset',
                'type' => 'text',
                'instructions' => 'pixels to move the content area vertically. PS: Negative values will move it further up',
                'default_value' => '0',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'none',
                'maxlength' => '5',
            ),

			array (
                'key' => 'field_524120fu38893123abo',
                'label' => 'Show or Hide Contents',
                'name' => 'show_contents',
                'type' => 'radio',
                'choices' => array (
                    'Show' => 'Show',
                    'Hide' => 'Hide',
                ),
                'other_choice' => 0,
                'save_other_choice' => 0,
                'default_value' => 'Show',
                'layout' => 'horizontal',
            ),  
            
            array (
                'key' => 'field_524010fu38943123abo',
                'label' => 'Show/Hide "Read More" button',
                'instructions' => 'Shows "Read More" button on top of each page/content area.',
                'name' => 'show_readmore',
                'type' => 'radio',
                'choices' => array (
                    'Show' => 'Show',
                    'Hide' => 'Hide',
                ),
                'other_choice' => 0,
                'save_other_choice' => 0,
                'default_value' => '',
                'layout' => 'horizontal',
            ),
                        
            array (
                'key' => 'field_524120fu38943123abo',
                'label' => 'Enable/Disable "Content Box"',
                'instructions' => 'Wraps all contents in a box',
                'name' => 'show_contentbox',
                'type' => 'radio',
                'choices' => array (
                    'Enable' => 'Enable',
                    'Disable' => 'Disable',
                ),
                'other_choice' => 0,
                'save_other_choice' => 0,
                'default_value' => 'Enable',
                'layout' => 'horizontal',
            ),     
            
            array (
                'key' => 'field_524120fu38961123abo',
                'label' => 'Enable/Disable "Page Title"',
                'instructions' => 'Show or Hide the title of the page',
                'name' => 'show_pagetitle',
                'type' => 'radio',
                'choices' => array (
                    'Show' => 'Show',
                    'Hide' => 'Hide',
                ),
                'other_choice' => 0,
                'save_other_choice' => 0,
                'default_value' => 'Show',
                'layout' => 'horizontal',
            ),                     
                                  
        ),
        'location' => array (
            array (
                array (
                    'param' => 'page_template',
                    'operator' => '==',
                    'value' => 'template-about.php',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        
        
        'menu_order' => 0,
    ));





/*=================================================
	Portfolio Content Type
==================================================*/

$portfolio_template_pages = array('default'=>'Default');
	//grab all pages that are using the portfolio layout
	$portfolio_template_list = get_pages(array(
		'meta_key' => '_wp_page_template',
		'meta_value' => 'template-portfolio.php',
	));
	
	if(!empty($portfolio_template_list)) {
		foreach($portfolio_template_list as $portfolioitem){
			$portfolio_template_pages[$portfolioitem->ID] = $portfolioitem->post_title;
		}
	}
    

    register_field_group(array (
        'id' => 'acf_project',
        'title' => 'Project',
        'fields' => array (
            array (
                'key' => 'field_52318680a5d00',
                'label' => 'Slider',
                'name' => 'slider',
                'type' => 'flexible_content',
                'layouts' => array (
                    array (
                        'label' => 'Image',
                        'name' => 'image',
                        'display' => 'row',
                        'sub_fields' => array (
                            array (
                                'key' => 'field_5231869fa5d01',
                                'label' => 'Image',
                                'name' => 'image',
                                'type' => 'image',
                                'column_width' => '',
                                'save_format' => 'object',
                                'preview_size' => 'thumbnail',
                                'library' => 'all',
                            ),
                        ),
                    ),
                    array (
                        'label' => 'Video',
                        'name' => 'video',
                        'display' => 'row',
                        'sub_fields' => array (
                            array (
                                'key' => 'field_523186afa5d03',
                                'label' => 'Video URL',
                                'name' => 'video_url',
                                'type' => 'text',
                                'instructions' => 'Type the video URL from Youtube or Vimeo',
                                'column_width' => '',
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'formatting' => 'none',
                                'maxlength' => '',
                            ),
                        ),
                    ),
                ),
                'button_label' => 'Add Row',
            ),
            array (
                'key' => 'field_523186d315b5d',
                'label' => 'Related Projects',
                'name' => 'related_projects',
                'type' => 'relationship',
                'instructions' => 'Click on projects which you think are related to this one, otherwise if you leave blank the system will try to generate related one\'s.',
                'return_format' => 'object',
                'post_type' => array (
                    0 => 'portfolio_item',
                ),
                'taxonomy' => array (
                    0 => 'all',
                ),
                'filters' => array (
                    0 => 'search',
                ),
                'result_elements' => array (
                    0 => 'post_type',
                    1 => 'post_title',
                ),
                'max' => '',
            ),
            
            
            array (
                'key' => 'field_portfoliobbacktoall',
                'label' => 'Back to all link',
                'name' => 'backto_all',
                'type' => 'select',
					'choices' => $portfolio_template_pages,
					'std' => 'default',
                'instructions' => 'This allows you to manually assign where your "Show all" button will take the user on your single portfolio item pages.',
            ),            
            
            
        ),
        
        
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'portfolio_item',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
            
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));

}

?>