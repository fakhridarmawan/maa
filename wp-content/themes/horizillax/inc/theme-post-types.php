<?php
function convax_post_types() {
	register_post_type( 'portfolio_item',
		array(
			'labels' => array(
				'name' => 'Portfolio',
				'singular_name' =>'Project',
				'add_new' => 'Add New Project',
				'add_new_item' => 'Add New Project',
				'edit' => 'Edit Project',
				'edit_item' =>'Edit Project',
			),
			'description' => 'Portfolio Items.',
			'public' => true,
			'supports' => array( 'title', 'editor', 'thumbnail' ),
			'rewrite' => array( 'slug' => 'portfolio_post', 'with_front' => false ),
			'has_archive' => true
		)
	);
	register_taxonomy( 'portfolio_category', array( 'portfolio_item' ),
	 
	array( 'hierarchical' => true, 'label' => "Categories","singular_label" => "Category" ) );
	
}
?>