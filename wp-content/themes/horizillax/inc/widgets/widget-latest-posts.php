<?php
/** Recent Posts Widget */
class widget_recent_posts extends WP_Widget {
function widget_recent_posts() {
parent::WP_Widget(false, $name = 'Recent Projects');	
}
function widget($args, $instance) {	
extract( $args );
global $posttypes;
$title 			= apply_filters('widget_title', $instance['title']);
$numb 		= apply_filters('widget_title', $instance['number']);
$thumb 		= $instance['thumbnail'];
$posttype 		= $instance['posttype'];
?>
<?php echo $before_widget; ?>
<?php if ( $title !=="" ){
echo $before_title . $title . $after_title;}
else{
echo $before_title . 'Recent Projects' . $after_title;
}
 ?>
<div id="recent-projects">
<ul class="<?php echo $posttype.'-widget'?> recent-project-list">
<?php
global $post;
$tmp_post = $post;
$args = 'numberposts=' . $numb . '&post_type=' . $posttype;
$myposts = get_posts( $args );
foreach( $myposts as $post ) : setup_postdata($post); ?>
<li>
<a href="<?php the_permalink(); ?>" class="<?php if($thumb == true && has_post_thumbnail()) echo 'overlay' ?>">
<?php if($thumb == true) { ?>
<?php 
if(has_post_thumbnail()) {
the_post_thumbnail('portfolio_medium');
} 
?>
<?php } ?>
<span class="icon-align"><?php the_title(); ?></span>
</a>

</li>
<?php endforeach; ?>
<?php $post = $tmp_post; ?>
</ul>
</div>
<?php echo $after_widget; ?>
<?php
}
function update($new_instance, $old_instance) {		
global $posttypes;
$instance = $old_instance;
$instance['title'] = strip_tags($new_instance['title']);
$instance['number'] = strip_tags($new_instance['number']);
$instance['thumbnail'] = $new_instance['thumbnail'];
$instance['posttype'] = $new_instance['posttype'];
return $instance;
}

function form($instance) {	
$posttypes = get_post_types('', 'objects');
$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
$numb = isset( $instance['number'] ) ? esc_attr( $instance['number'] ) : '';
$thumb = isset( $instance['thumbnail'] ) ? esc_attr( $instance['thumbnail'] ) : '';
$posttype	= isset( $instance['posttype'] ) ? esc_attr( $instance['posttype'] ) : '';
?>
<p>
<label for="<?php echo $this->get_field_id('title'); ?>">Title:</label> 
<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
</p>
<p>
<label for="<?php echo $this->get_field_id('posttype'); ?>">Post Type to show</label> 
<select name="<?php echo $this->get_field_name('posttype'); ?>" id="<?php echo $this->get_field_id('posttype'); ?>" class="widefat">
<?php foreach($posttypes as $option) : ?>
<?php if ($option->name !=='revision' && $option->name !=='attachment' && $option->name !=='options' && $option->name !=='acf' && $option->name !=='nav_menu_item' && $option->name !=='page') : ?>
<?php	echo '<option value="' . $option->name . '" id="' . $option->name . '"', $posttype == $option->name ? ' selected="selected"' : '', '>', $option->name, '</option>'; ?>
<?php endif; ?>
<?php endforeach; ?>
</select></p>
<p>
<label for="<?php echo $this->get_field_id('number'); ?>">Number of items:</label> 
<input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $numb; ?>" />
</p>
<p>
<input id="<?php echo $this->get_field_id('thumbnail'); ?>" name="<?php echo $this->get_field_name('thumbnail'); ?>" type="checkbox" value="1" <?php checked( '1', $thumb ); ?>/>
<label for="<?php echo $this->get_field_id('thumbnail'); ?>">Display thumbnails?</label> 
</p>		
<?php 
}
} 
register_widget('widget_recent_posts');
?>