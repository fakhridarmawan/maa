<?php
/**
 * Registers two widget areas.
 */
	function productivethemes_widgets_init() {

		register_sidebar( array(
			'name'          => __( 'Sidebar Widget Area', 'productivethemes' ),
			'id'            => 'sidebar-2',
			'description'   => __( 'Appears on posts and pages in the sidebar.', 'productivethemes' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
	}
	add_action( 'widgets_init', 'productivethemes_widgets_init' );
	
	require_once "widget-latest-posts.php";
	require_once "widget-social-networks.php";
?>