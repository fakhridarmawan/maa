<?php
/** Social Networks Widget*/
class widget_social_network extends WP_Widget {
function widget_social_network() {
$widget_ops = array('classname' => 'social_networks_widget', 'description' => 'Link to your social networks.');
$this->WP_Widget('social_networks', 'Social Networks', $widget_ops);
}
function widget( $args, $instance ) {
extract($args);
$title = apply_filters('widget_title', $instance['title']);
$socialnetwork['twitter'] 		= $instance['twitter'];
$socialnetwork['facebook'] 		= $instance['facebook'];
$socialnetwork['pinterest']		= $instance['pinterest'];
$socialnetwork['linkedin']	 	= $instance['linkedin'];
$socialnetwork['flickr'] 		= $instance['flickr'];
$socialnetwork['dribbble'] 		= $instance['dribbble'];
$socialnetwork['youtube'] 		= $instance['youtube'];
$socialnetwork['vimeo'] 		= $instance['vimeo'];
$socialnetwork['google-plus'] 	= $instance['google-plus'];
$socialnetwork['tumblr'] 		= $instance['tumblr'];
$socialnetwork['instagram'] 	= $instance['instagram'];
$socialnetwork['github'] 		= $instance['github'];


echo $before_widget;
if ($title !== ''){
echo $before_title;
echo($title); 
echo $after_title;
}

?>

<?php  global $smof_plugin_data; foreach(array("twitter", "facebook", "pinterest", "linkedin", "flickr", "youtube", "dribbble", "vimeo", "google-plus", "tumblr","instagram", "github") as $network) : ?>
<?php if($socialnetwork[$network] == 1): 
	if(isset($network) && $network == "google-plus"){
		$socialurl = $smof_plugin_data['googleplus_url'];
	}
	else{
		$socialurl = $smof_plugin_data[$network.'_url'];
	}
?>
<a class="btn btn-social-icon btn-sm btn-<?php echo $network; ?>" href="<?php echo $socialurl; ?>">
<i class="fa fa-<?php if($network == "vimeo"){echo $network.'-square';}else{echo $network;}; ?>"></i></a>
<?php endif; ?>
<?php endforeach; ?>

<?php
echo $after_widget;
}
function update( $new_instance, $old_instance ) {
$instance = $old_instance;
$instance['title'] = strip_tags($new_instance['title']);
$instance['twitter'] = $new_instance['twitter'];
$instance['facebook'] = $new_instance['facebook'];
$instance['pinterest'] = $new_instance['pinterest'];
$instance['linkedin'] = $new_instance['linkedin'];
$instance['flickr'] = $new_instance['flickr'];
$instance['youtube'] = $new_instance['youtube'];
$instance['dribbble'] = $new_instance['dribbble'];
$instance['vimeo'] = $new_instance['vimeo'];
$instance['google-plus'] = $new_instance['google-plus'];
$instance['tumblr'] = $new_instance['tumblr'];
$instance['instagram'] = $new_instance['instagram'];
$instance['github'] = $new_instance['github'];
return $instance;
}
function form( $instance ) {
$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'text' => '') );
$title = strip_tags($instance['title']);
$twitter = isset( $instance['twitter'] ) ? esc_attr( $instance['twitter'] ) : '';
$pinterest = isset( $instance['pinterest'] ) ? esc_attr( $instance['pinterest'] ) : '';		
$facebook = isset( $instance['facebook'] ) ? esc_attr( $instance['facebook'] ) : '';	
$linkedin = isset( $instance['linkedin'] ) ? esc_attr( $instance['linkedin'] ) : '';	
$flickr =  isset( $instance['flickr'] ) ? esc_attr( $instance['flickr'] ) : '';	
$youtube = isset( $instance['youtube'] ) ? esc_attr( $instance['youtube'] ) : '';	
$dribbble = isset( $instance['dribbble'] ) ? esc_attr( $instance['dribbble'] ) : '';		
$vimeo =  isset( $instance['vimeo'] ) ? esc_attr( $instance['vimeo'] ) : '';
$googleplus = isset( $instance['google-plus'] ) ? esc_attr( $instance['google-plus'] ) : '';
$tumblr = isset( $instance['tumblr'] ) ? esc_attr( $instance['tumblr'] ) : '';
$instagram = isset( $instance['instagram'] ) ? esc_attr( $instance['instagram'] ) : '';	
$github = isset( $instance['github'] ) ? esc_attr( $instance['github'] ) : '';															
$text = format_to_edit($instance['text']);
?>
<p><label for="<?php echo $this->get_field_id('title'); ?>">Title</label>
<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>
<p><label for="<?php echo $this->get_field_id('twitter'); ?>">Twitter URL:</label>
<input id="<?php echo $this->get_field_id('twitter'); ?>" name="<?php echo $this->get_field_name('twitter'); ?>" type="checkbox" value="1" <?php checked( '1', $twitter ); ?>/>
<p><label for="<?php echo $this->get_field_id('facebook'); ?>">Facebook URL:</label>
<input id="<?php echo $this->get_field_id('facebook'); ?>" name="<?php echo $this->get_field_name('facebook'); ?>" type="checkbox" value="1" <?php checked( '1', $facebook ); ?>/>
<p><label for="<?php echo $this->get_field_id('pinterest'); ?>">Pinterest URL:</label>
<input id="<?php echo $this->get_field_id('pinterest'); ?>" name="<?php echo $this->get_field_name('pinterest'); ?>" type="checkbox" value="1" <?php checked( '1', $pinterest ); ?>/>
<p><label for="<?php echo $this->get_field_id('linkedin'); ?>">Linkedin URL:</label>
<input id="<?php echo $this->get_field_id('linkedin'); ?>" name="<?php echo $this->get_field_name('linkedin'); ?>" type="checkbox" value="1" <?php checked( '1', $linkedin ); ?>/>
<p><label for="<?php echo $this->get_field_id('flickr'); ?>">Flickr URL:</label>
<input id="<?php echo $this->get_field_id('flickr'); ?>" name="<?php echo $this->get_field_name('flickr'); ?>" type="checkbox" value="1" <?php checked( '1', $flickr ); ?>/>
<p><label for="<?php echo $this->get_field_id('youtube'); ?>">Youtube URL:</label>
<input id="<?php echo $this->get_field_id('youtube'); ?>" name="<?php echo $this->get_field_name('youtube'); ?>" type="checkbox" value="1" <?php checked( '1', $youtube ); ?>/>
<p><label for="<?php echo $this->get_field_id('dribbble'); ?>">Dribbble URL:</label>
<input id="<?php echo $this->get_field_id('dribbble'); ?>" name="<?php echo $this->get_field_name('dribbble'); ?>" type="checkbox" value="1" <?php checked( '1', $dribbble ); ?>/>
<p><label for="<?php echo $this->get_field_id('vimeo'); ?>">Vimeo URL:</label>
<input id="<?php echo $this->get_field_id('vimeo'); ?>" name="<?php echo $this->get_field_name('vimeo'); ?>" type="checkbox" value="1" <?php checked( '1', $vimeo ); ?>/>
<p><label for="<?php echo $this->get_field_id('google-plus'); ?>">Google Plus URL:</label>
<input id="<?php echo $this->get_field_id('google-plus'); ?>" name="<?php echo $this->get_field_name('google-plus'); ?>" type="checkbox" value="1" <?php checked( '1', $googleplus ); ?>/>
<p><label for="<?php echo $this->get_field_id('tumblr'); ?>">Tumblr URL:</label>
<input id="<?php echo $this->get_field_id('tumblr'); ?>" name="<?php echo $this->get_field_name('tumblr'); ?>" type="checkbox" value="1" <?php checked( '1', $tumblr ); ?>/>
<p><label for="<?php echo $this->get_field_id('instagram'); ?>">Instagram URL:</label>
<input id="<?php echo $this->get_field_id('instagram'); ?>" name="<?php echo $this->get_field_name('instagram'); ?>" type="checkbox" value="1" <?php checked( '1', $instagram ); ?>/>
<p><label for="<?php echo $this->get_field_id('github'); ?>">Github URL:</label>
<input id="<?php echo $this->get_field_id('github'); ?>" name="<?php echo $this->get_field_name('github'); ?>" type="checkbox" value="1" <?php checked( '1', $github ); ?>/>
<?php
}
}
register_widget('widget_social_network');



?>