<?php get_header(); ?>

<div class="container">
	<div class="row">
	            <div class="page-title"><h1><?php if(!is_front_page()){echo apply_filters( 'the_title', get_the_title( get_option( 'page_for_posts' ) ) );}
 ?></h1>
					<?php if(get_field("heading_title")): ?>
                    	<h4><?php the_field("heading_title"); ?></h4>
                    <?php endif; ?>
                    <?php if(get_field("sub_heading")): ?>
                    	<div class="page-desc"><?php the_field("sub_heading"); ?></div>
                     <?php endif; ?>
                     
            
            </div>

	
		<div id="primary" class="blog-main <?php if(!is_dynamic_sidebar('sidebar-2 ')){echo'no-sidebar ';} if(isset($smof_plugin_data['responsive']) && $smof_plugin_data['responsive'] == 0){ echo 'col-xs-9 ';}else{echo 'col-sm-9';}?>">
			<div id="content" class="site-content" role="main">
				<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'content' ); ?>
						<?php endwhile; ?>
					<?php if (function_exists("horizillax_pagination")) { horizillax_pagination();}?>
					<?php else : ?>
						<?php get_template_part( 'content', 'none' ); ?>
					<?php endif; ?>
			</div><!-- #content -->
		</div><!-- #primary -->
<?php get_sidebar(); ?>
	</div>	<!--.row -->	
</div> <!--.container -->	

<?php get_footer(); ?>