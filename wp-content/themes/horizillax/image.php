<?php get_header(); 


	if ( ! function_exists( 'productivethemes_the_attached_image' ) ) :
	/**
 	* Prints the attached image with a link to the next attached image.
 	*/
		function productivethemes_the_attached_image() {
			$post                = get_post();
			$attachment_size     = apply_filters( 'productivethemes_attachment_size', array( 724, 724 ) );
			$next_attachment_url = wp_get_attachment_url();
			/**
			* Grab the IDs of all the image attachments in a gallery.
			*/
			$attachment_ids = get_posts( array(
				'post_parent'    => $post->post_parent,
				'fields'         => 'ids',
				'numberposts'    => -1,
				'post_status'    => 'inherit',
				'post_type'      => 'attachment',
				'post_mime_type' => 'image',
				'order'          => 'ASC',
				'orderby'        => 'menu_order ID'
			) );

			// If there is more than 1 attachment in a gallery…
			if ( count( $attachment_ids ) > 1 ) {
				foreach ( $attachment_ids as $attachment_id ) {
					if ( $attachment_id == $post->ID ) {
						$next_id = current( $attachment_ids );
						break;
					}
				}
				// get the URL of the next image attachment…
				if ( $next_id )
					$next_attachment_url = get_attachment_link( $next_id );
				// or get the URL of the first image attachment.
				else
					$next_attachment_url = get_attachment_link( array_shift( $attachment_ids ) );
			}

			printf( '<a href="%1$s" title="%2$s" rel="attachment">%3$s</a>',
				esc_url( $next_attachment_url ),
				the_title_attribute( array( 'echo' => false ) ),
				wp_get_attachment_image( $post->ID, $attachment_size )
			);
		}
	endif;

?>
<div class="container">
	<div class="row">
				<article id="post-<?php the_ID(); ?>" <?php post_class( 'image-attachment' ); ?>>
					<header class="entry-header">
						<h1 class="entry-title"><?php the_title(); ?></h1>
						<div class="entry-meta">
							<?php
								$published_text = __( '<span class="attachment-meta">Published on <time datetime="%1$s">%2$s</time> in <a href="%3$s" title="Return to %4$s" rel="gallery">%5$s</a></span>', 'productivethemes' );
								$post_title = get_the_title( $post->post_parent );
								if ( empty( $post_title ) || 0 == $post->post_parent )
									$published_text = '<span class="attachment-meta"><time datetime="%1$s">%2$s</time></span>';
								printf( $published_text,
									esc_attr( get_the_date( 'c' ) ),
									esc_html( get_the_date() ),
									esc_url( get_permalink( $post->post_parent ) ),
									esc_attr( strip_tags( $post_title ) ),
									$post_title
								);
								$metadata = wp_get_attachment_metadata();
								printf( '<span class="attachment-meta full-size-link"><a href="%1$s" title="%2$s">%3$s (%4$s &times; %5$s)</a></span>',
									esc_url( wp_get_attachment_url() ),
									esc_attr__( 'Link to full-size image', 'productivethemes' ),
									__( 'Full resolution', 'productivethemes' ),
									$metadata['width'],
									$metadata['height']
								);
								edit_post_link( __( 'Edit', 'productivethemes' ), '<span class="edit-link">', '</span>' );
							?>
						</div><!-- .entry-meta -->
					</header><!-- .entry-header -->
					<div class="entry-content">
							<div class="attachment">
                       			<div class="bx-controls-direction btn-group btn-group-expand">
                           			<?php previous_image_link( false, __( '<i class="fa fa-chevron-left"></i>', 'productivethemes' ) ); ?>
                           			<?php next_image_link( false, __( '<i class="fa fa-chevron-right"></i>', 'productivethemes' ) ); ?>
                        		</div><!-- #image-navigation -->							
								<?php productivethemes_the_attached_image(); ?>

								<?php if ( has_excerpt() ) : ?>
									<div class="entry-caption">
									<?php the_excerpt(); ?>
									</div>
								<?php endif; ?>
							</div><!-- .attachment -->

						<?php if ( ! empty( $post->post_content ) ) : ?>
							<div class="entry-description">
								<?php the_content(); ?>
								<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'productivethemes' ), 'after' => '</div>' ) ); ?>
							</div><!-- .entry-description -->
						<?php endif; ?>
					</div><!-- .entry-content -->
				</article><!-- #post -->
				<?php comments_template(); ?>
			</div><!-- #content -->
		</div><!-- .row -->
<?php get_footer(); ?>