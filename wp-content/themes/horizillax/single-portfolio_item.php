<?php get_header(); ?>
<?php global $smof_plugin_data; ?>

<div class="container">
	<div class="row">
	
	
	
	
		<div class="col-sm-9 <?php if(isset($data['responsive']) && !$data['responsive'] == 1){ echo 'col-xs-9 col-md-9';}?>">
                <?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				 
				
	<header class="entry-header">
				<h1 class="entry-title"><?php the_title(); ?></h1>
			
			
			
		<?php
                	$terms = wp_get_post_terms( $post->ID,"portfolio_category" );
                    $terms_html_array = array();
                    $terms_id_array = array();
                    $term_classes = "";
                    foreach($terms as $t){
                    	$term_name = $t->name;
                        $term_link = get_term_link($t->slug,$t->taxonomy);
                        array_push($terms_html_array,"<a href='{$term_link}'>{$term_name}</a>");
                        array_push($terms_id_array,$t->slug);
                        $term_classes .= "pt_".$t->slug." ";
                    }
                    $terms_html_array = implode(" / ",$terms_html_array);
                    
                    
                    if($terms_html_array){
                    echo '<div class="cat-tag">
					<div class="categories_list">
					<i class="fa fa-folder-open"></i>'.
                     $terms_html_array.' </div>
            		</div>';
            		}
            ?>
           
			
			<?php edit_post_link( __( 'Edit', 'productivethemes' ), '<span class="edit-link">', '</span>' ); ?>
			
			
	</header><!-- .entry-header -->
	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
		<div class="entry-summary">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
	<?php else : ?>
				<div class="entry-meta single-portfolio-slider">
				
			<?php if(get_field("slider")){?>
            	<ul id="portslider">
                	<?php while(has_sub_field("slider")): ?>
                   		<?php if(get_row_layout() == "image"): $image = get_sub_field("image"); ?>
                        	<li><img src="<?php echo $image["sizes"]["large"]; ?>" alt="<?php echo $image["alt"]; ?>"></li>
                   		<?php elseif(get_row_layout() == "video"): ?>
                        	<li><?php echo horizillax_video_embed(get_sub_field("video_url"),"693px","100%"); ?></li>
                    	<?php endif; ?>
                	<?php endwhile; ?>
            	</ul>
            <?php } else { ?>	

				
					<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
						<?php the_post_thumbnail('portfolio_large'); ?>
						</div>
					<?php endif; ?>
					
			<?php } ?>		
				</div><!-- .entry-meta -->
			<?php if(get_the_content()){?>	
			<div class="entry-content">	
			<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'productivethemes' ) ); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'productivethemes' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
			</div>
			<?php } ?>
	<?php endif; ?>
	<div class="btn-group-wrap">
	<?php horizillax_post_nav(); ?>
	</div>
</article><!-- #post -->




                   
                <?php endwhile; ?>
                <?php
            		$related_posts = get_field("related_projects");
            		if(!$related_posts){
						$args = array();
						$args["post_not_in"] = $post->ID;
						$args["posts_per_page"] = -1;
						$args["post_type"] = "portfolio_item";
						$args["tax_query"] = array('relation' => 'OR');
		                /*Tags And Category*/
						$category = wp_get_post_terms( $post->ID,"portfolio_category" );
						$category_array = array();
						if($category){
                    		foreach($category as $tag){
                        		array_push($category_array,$tag->slug);
							}
                    		array_push($args["tax_query"],array(
                       		'taxonomy' => 'portfolio_category',
                        	'field' => 'slug',
                        	'terms' => $category_array
                    	));
                		}
           	 		}
            	if($related_posts):
				?>
						<?php if(isset($smof_plugin_data['related_projects_title']) && !empty($smof_plugin_data['related_projects_title'])){ 
                        $related_title = '<h2>'.$smof_plugin_data['related_projects_title'].'</h2>'; 
                        $related_title_class="has_title";
                        }else{
                        $related_title ='';
                        $related_title_class='has_no_title';
                        }
                        
                        ?>
				
        			<div class="related-items content-box <?php echo $related_title_class; ?>">
						<?php echo $related_title; ?>
            			<ul class="recent-item-list">
            			<?php
							foreach($related_posts as $post):
							setup_postdata($post);?>
                            <li>
                                    <div class="overlay">
										<?php 
                                        if(has_post_thumbnail()) {
                                        	the_post_thumbnail('portfolio_medium');
                                        	$large_image    = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
                                        } else {
                                        	echo '<a href="'.the_permalink().'"><img src="'.get_bloginfo("template_url").'/images/default-featured-image.png" width="200" height="200" /></a>';
                                        }
                                        ?>
                                        <span class="icon-align two-icons">
                                            <a class="icon magnify" title="<?php the_title_attribute(); ?>" href="<?php echo $large_image[0];?>"><i class="fa fa-search"></i></a>
                                            <a class="icon" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><i class="fa fa-link"></i></a>
                                        </span><!-- .icon-align -->
                                     </div><!-- .overlay -->
                                     <div class="entry-header small-title"><h1 class="blog-post-title"> <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1></div>
                                    
                            </li>
    
                			<?php endforeach;
                		
               			wp_reset_postdata();?>
            			</ul>
        			</div>
        		<?php endif; ?><!-- .related-items -->
        		</div>
	</div>	<!--.row -->	
</div> <!--.container -->	
<?php get_footer(); ?>