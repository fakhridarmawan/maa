<?php
/*
Template Name: Parallax
*/
get_header("parallax"); global $smof_plugin_data;?> 

<div id="parallax-container">
<?php
$values =  get_pages('sort_column=menu_order&post_type=page&post_status=publish');
$postspage_id = get_option('page_for_posts'); 
$i = 0;
foreach ( $values as $post ) {

	$new_title = str_replace( " ", "_", strtolower( $post->post_title ) ).$i;
	$tempname = get_post_meta( $post->ID, '_wp_page_template', true );
	$filename = preg_replace('"\.php$"', '', $tempname);
	$contents = '';
	$titles='';
	$scrollbar="";
	$scrollbar_content="";
	$show_readmore ="";

	
	
	if(get_field("show_pagetitle")=="Show" || get_field("show_pagetitle")==""){
		$pagetitle = '<h1>'.$post->post_title.'</h1>';
	}
	else{
		$pagetitle = '';
	}
		
	//Show Subtitles
	if (get_field("heading_title", $post->ID)!==""){
		$titles='<h4>'.get_field("heading_title", $post->ID).'</h4>';
	}


	if(get_field("show_contentbox")=="Enable" || get_field("show_contentbox")==""){
		$open_content_div = '<div class="content-box">';
		$close_content_div = '</div>';
	}
	else{
		$open_content_div = '';
		$close_content_div = '';
	}
	
	if(get_field("show_contents")=="Show" || get_field("show_contents")==""){
		$show_contents=true;
	}
	else{
		$show_contents=false;
	}
	
	
	
	// If Blog Page
	if($post->ID == $postspage_id){
		$scrollbar="nano";
		$scrollbar_content="parallax-blog nano-content";
		
		if(get_field("show_readmore")=="Show" || get_field("show_readmore")==""){
			$show_readmore = '<a href="'.get_permalink($post->ID).'" class="btn btn-default link" data-original-title="SHOW ALL"><i class="fa fa-bars"></i></a>';
		}
		
		$contents.='<div class="blogpage">';
		$postloop = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 5, 'orderby'=>'date', 'paged' => $paged ) );
		while ($postloop->have_posts()) : $postloop->the_post(); 
			$short_title = the_title('','',false);
			if(strlen($short_title) > 75){
			$short_title = substr($short_title, 0, 75) . "…";
			}
		$blogpostclass=	get_post_class();
		$contents.= '
		<div class="blogpost '.$blogpostclass[4].'">
			<div class="post_format fa"></div>
			<header class="entry-header small-title">
				<h1 class="blog-post-title"><a href="'.get_permalink().'">'.$short_title.'</a></h1>
				<div class="meta-details">
					<a href="'.get_author_posts_url( get_the_author_meta( 'ID' )).'"><i class="fa-user fa"></i> '.get_the_author().'</a>
					<a href="'.get_permalink().'"><i class="fa-clock-o fa"></i> '.get_the_date('M d, Y').'</a>
					<a href="'.get_permalink().'#comments"><i class="fa-comment fa"></i> '.get_comments_number().'</a>
				</div>
			</header>';
		if(has_post_thumbnail()) {
			$contents.=  '
			<a href="'.get_permalink().'" class="overlay"><span class="icon-align"><span class="icon"><i class="fa-link fa"></i></span></span>'.get_the_post_thumbnail(get_the_ID() ,'blog_parallax').'</a>';
		}
		else{
			$contents.=  '
			<div class="entry-content">'.excerpt('25').'</div>';
		}
		$contents.= '</div>';
 		endwhile; wp_reset_query();
		$contents.= '</div>'; 
 	}
 
 
	//If Portfolio Template
	elseif($filename == 'template-portfolio'){
		$arguments = array();
		$arguments["post_type"] = "portfolio_item";
		$arguments["orderby"] = "date";
		$arguments["posts_per_page"] = get_field("num_projects");
		if(get_field("include_only_those_categories") || get_field("exclude_categories")){
			$arguments["tax_query"] = array();
			if(get_field("include_only_those_categories") && get_field("exclude_categories")){
				$arguments["tax_query"]['relation'] = 'OR';
			}
			if(get_field("exclude_categories")){
				$exclude_categories = explode(",",get_field("exclude_categories"));
				array_push($arguments["tax_query"],array(
					'taxonomy' => 'portfolio_category',
					'field' => 'slug',
					'terms' => $exclude_categories,
					'operator' => 'NOT IN'
				));
			}
			if(get_field("include_only_those_categories")){
				$include_categories = explode(",",get_field("include_only_those_categories"));
				array_push($arguments["tax_query"],array(
					'taxonomy' => 'portfolio_category',
					'field' => 'slug',
					'terms' => $include_categories,
					'operator' => 'IN'
				));
			}
		}
                        
		if(get_field("exclude_categories")){
			$exlude_posts = array();
			foreach(get_field("exclude_categories") as $tmpPost){
				array_push($exlude_posts,$tmpPost->ID);
			}
			$arguments["post__not_in"] = $exlude_posts;
		}
        
		if(get_field("exclude_posts")){
			$exlude_posts = array();
			foreach(get_field("exclude_posts") as $tmpPost){
				array_push($exlude_posts,$tmpPost->ID);
			}
			$arguments["post__not_in"] = $exlude_posts;
		}


		$arguments["post_status"] = "publish";
		
	if(get_field("show_readmore")=="Show" || get_field("show_readmore")==""){
			$show_readmore = '<a href="'.get_permalink($post->ID).'" class="btn btn-default link" data-original-title="SHOW ALL"><i class="fa fa-bars"></i></a>';
		}
	
		/* Portfolio Filter */
	
		if(get_field("include_only_those_categories") || get_field("exclude_categories")){
    		$arguments["tax_query"] = array();
			if(get_field("include_only_those_categories") && get_field("exclude_categories")){
				$arguments["tax_query"]['relation'] = 'OR';
			}
			if(get_field("exclude_categories")){
				$exclude_categories = explode(",",get_field("exclude_categories"));
				array_push($arguments["tax_query"],array(
					'taxonomy' => 'portfolio_category',
					'field' => 'slug',
					'terms' => $exclude_categories,
					'operator' => 'NOT IN'
				));
			}
			if(get_field("include_only_those_categories")){
				$include_categories = explode(",",get_field("include_only_those_categories"));
				array_push($arguments["tax_query"],array(
					'taxonomy' => 'portfolio_category',
					'field' => 'slug',
					'terms' => $include_categories,
					'operator' => 'IN'
				));
			}
		}
                                
		if(get_field("exclude_posts")){
			$exlude_posts = array();
			foreach(get_field("exclude_posts") as $tmpPost){
				array_push($exlude_posts,$tmpPost->ID);
			}
			$arguments["post__not_in"] = $exlude_posts;
		}

		$portloop = new WP_Query( $arguments );
		$small_image ="";
		$large_image ="";
		$scrollbar="nano";
		$scrollbar_content="parallax-portfolio nano-content";
		$portcount = 0;
		
		while ($portloop->have_posts()) : $portloop->the_post(); 
			$portfolio_contents = '<div class="contents">
										<h2><a href="'.get_permalink().'">'.get_the_title().'</a></h2>
										<p>'.excerpt("28").'</p>
										<a href="'.get_permalink().'" class="btn btn-default btn-icon"><i class="fa fa-link"></i> Read More</a>
									</div>';
	
			if(has_post_thumbnail()) {
				$round_thumb_image = get_the_post_thumbnail(get_the_ID() ,"thumbnail");
				$large_image.= '<li><a href="'.get_permalink().'" class="attachent-container">'.get_the_post_thumbnail(get_the_ID() ,"portfolio_medium").'</a>'.$portfolio_contents.'</li>';
				$small_image.= '<a href="" data-slide-index="'.$portcount.'"  class="attachent-container">'.$round_thumb_image.'</a>';
			} else {
				$large_image.= '<li><a href="'.get_permalink().'"><img src="'.get_bloginfo("template_url").'/images/default-featured-image.png" width="200" height="200" /></a>'.$portfolio_contents.'</li>';
				$small_image.= '<a data-slide-index="'.$portcount.'" href=""><img src="'.get_bloginfo("template_url").'/images/default-featured-image.png" width="500" height="500" /></a>';
			}
			
		$portcount++;
		endwhile;
	
		$contents.='<ul class="slider">'.$large_image.'</ul>';	
		$contents.='<div id="pager">'.$small_image.'</div>';
	
		wp_reset_postdata();
	}
 
	//If About Template
	elseif($filename == 'template-about'){
		$scrollbar_content="parallax-about nano-content";
		$scrollbar="nano";
		
		if(get_field("show_readmore")=="Show" || get_field("show_readmore")==""){
			$show_readmore = '<a href="'.get_permalink($post->ID).'" class="btn btn-default link" data-original-title="SHOW ALL"><i class="fa fa-bars"></i></a>';
		}
		
		$members = get_field("team_members", $post->ID);
		if($members){
			$contents.='<div class="team">';
				
        		foreach($members as $member){
					$contents.= '<div class="member">';
						if($member["member_avatar"]){
       						$contents.= '
       						<div class="team-group">
       							<div class="team-pic">
       								<img src="'.$member["member_avatar"]["sizes"]["team_member"].'" alt="'.$member["member_avatar"]["alt"].'" class="image-height">
       							</div>
       							<div class="team-desc image-height">
       								<div class="wrapper-pad">'.$member["member_description"].'</div>
       							</div>
       						</div>';
						}
					$contents.= '<div class="team-content"><h3>'.$member["member_name"].'</h3>';
					if($member["member_designation"]){
       					$contents.= '<p>'.$member["member_designation"].'</p>';
       				}
					$contents.= '<div class="member-social">';
					if($member["about_twitter"]){
						$contents.= '<a href="'.$member["about_twitter"].'"  class="btn btn-social-icon btn-sm btn-twitter"><i class="fa fa-twitter"></i></a>';
					}
        			if($member["about_facebook"]){
        				$contents.='<a href="'.$member["about_facebook"].'" class="btn btn-social-icon btn-sm btn-facebook"><i class="fa fa-facebook"></i></a>';
        			}
        			if($member["about_linkedin"]){
        				$contents.='<a href="'.$member["about_linkedin"].'" class="btn btn-social-icon btn-sm btn-linkedin"><i class="fa fa-linkedin"></i></a>';
        			}
       				$contents.='</div>';
					$contents.= '</div></div>';
        		}
       		$contents.= '</div>';
		}
		else{
		$contents.= '<div class="team"><h4>No Team Members added.</h4></div>';
		}

	}//END If About Template
				
				
	//If Parallax Template			
	elseif($filename == 'template-parallax'){
		$show_readmore='';
		$scrollbar_content="parallax-parallax nano-content";
		$scrollbar="nano";
		$contents = apply_filters('the_content',$post->post_content);
	}

				
	//If Default Template
	else{
		$contents = apply_filters('the_content',$post->post_content);
		$scrollbar="nano";
		$scrollbar_content="nano-content";
		if(get_field("show_readmore")=="Show" || get_field("show_readmore")==""){
		$show_readmore='<a href="'.get_permalink($post->ID).'" class="btn btn-default link" data-original-title="READMORE"><i class="fa fa-bars"></i></a>';
		}
	}//END If Default Template
	
	
		
	echo '<div id="'.$new_title.'" class="parallax-page">';
	if($show_contents){
		echo $open_content_div.'
		<div class="page-title">'.$pagetitle.$titles.'</div>
		<div class="page-contents '.$scrollbar.'">
			<div class="wrapper-pad '.$scrollbar_content.'">'.$contents.'</div>
		</div>'.$show_readmore.$close_content_div;
	}
	echo '</div>';
$i++;
}//end foreach

wp_reset_postdata();
?>

</div>

<?php get_footer(); ?>