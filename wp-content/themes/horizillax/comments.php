<?php if ( post_password_required() ) return;?>

	<?php if ( have_comments() ) : ?>
	<div id="comments" class="comments-area">
		<h2 class="comments-title">
			<?php
				printf( _nx( 'One comment', '%1$s comments', get_comments_number(), 'comments title', 'productivethemes' ),
				number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' );
			?>
		</h2>
		<ol class="comment-list">
			<?php
				wp_list_comments( array(
					'style'       => 'ol',
					'short_ping'  => true,
					'avatar_size' => 74,
				) );
			?>
		</ol><!-- .comment-list -->
		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :?>
			<nav class="navigation comment-navigation" role="navigation">
				<h1 class="screen-reader-text section-heading"><?php _e( 'Comment navigation', 'productivethemes' ); ?></h1>
				<div class="btn-group">
				<div class="nav-previous btn"><?php previous_comments_link( __( '&larr; Older Comments', 'productivethemes' ) ); ?></div>
				<div class="nav-next btn"><?php next_comments_link( __( 'Newer Comments &rarr;', 'productivethemes' ) ); ?></div>
				</div>
			</nav><!-- .comment-navigation -->
		<?php endif; ?>
		<?php if ( ! comments_open() && get_comments_number() ) : ?>
			<p class="no-comments">Comments are closed.</p>
		<?php endif; ?>
		</div><!-- #comments -->
	<?php endif; ?>
	<?php comment_form(); ?>
