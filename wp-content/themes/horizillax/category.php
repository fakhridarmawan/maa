<?php get_header(); ?>
<div class="container">
	<div class="row">
				<?php if ( have_posts() ) : ?>
					<div class="page-title"><?php printf( __( '<h4>Category Archives</h4> <h1>%s</h1>', 'productivethemes' ), single_cat_title( '', false ) ); ?>
						<?php if ( category_description() ) : // Show an optional category description ?>
							<div class="archive-meta">
								<?php echo category_description(); ?>
                            </div><!-- .archive-meta -->
						<?php endif; ?>
						
					</div><!-- .main-header -->
					
			<div id="primary" class="col-sm-9 <?php if(isset($data['responsive']) && !$data['responsive'] == 1){ echo 'col-xs-9 col-md-9';}?> blog-main">
				<div id="content" class="site-content" role="main">

					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'content' ); ?>
					<?php endwhile; ?>
				</div><!-- #content -->
				<?php if (function_exists("horizillax_pagination")) { horizillax_pagination();} ?>
			</div><!-- #primary -->
				<?php else : ?>
					<?php get_template_part( 'content', 'none' ); ?>
				<?php endif; ?>
				


<?php get_sidebar(); ?>
	</div>	<!--.row -->	
</div> <!--.container -->	
<?php get_footer(); ?>