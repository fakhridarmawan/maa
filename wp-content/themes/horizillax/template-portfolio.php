<?php
/*
Template Name: Portfolio
*/
get_header();
?>

<?php global $smof_plugin_data; ?>

        <div class="container">
				<div class="row">
            <div class="page-title">
            	<h1 class="main-title"><?php the_title(); ?></h1>
					<?php if(get_field("heading_title")): ?>
                	<h4><?php the_field("heading_title"); ?></h4>
               		<?php endif; ?>
					 <?php if(get_field("sub_heading")): ?>
                	<div class="page-desc"><?php the_field("sub_heading"); ?></div>
                	<?php endif; ?>
           </div><!-- .page-title -->
            	
            	
  
  
  
<?php  if(isset($smof_plugin_data['portfolio_filterable']) && $smof_plugin_data['portfolio_filterable'] == 1){ ?>  
  
            	
            	




            	
			<div id="portfolio-cat-filter" class="text-center">
					<ul class="pagination ">
					   <li><a href="#" data-filter="*" data-original-title="<?php echo wp_count_posts('portfolio_item')->publish; ?>" >All</a></li>
	               	   <?php wp_list_categories(array('title_li' => '', 'taxonomy' => 'portfolio_category', 'show_option_none'   => '', 'walker' => new Walker_Portfolio_Filter())); ?>
					</ul>
					<div class="clear"></div>
			</div>  	
            	
            	
            	
            	
            	
	<div class="portfolio-wrap">
			
			<span class="portfolio-preloader"></span>
						
			<div id="portfolio" class="portfolio-items isotope" data-categories-to-show="" data-starting-filter="" data-col-num="cols-3">
				<?php 
				if(isset($smof_plugin_data['portfolio_pagination']) && $smof_plugin_data['portfolio_pagination'] == 1){
				if(get_field("num_projects")){
				$posts_per_page = get_field("num_projects");
				}
				}
				else{
				$posts_per_page = -1;
				}
				$portfolio = array(
					'posts_per_page' => $posts_per_page,
					'post_type' => 'portfolio_item',
					'paged'=> $paged
				);
				
				$wp_query = new WP_Query($portfolio);
				
				if(have_posts()) : while(have_posts()) : the_post(); ?>
					
					<?php 
					
					   $terms = get_the_terms($post->id,"portfolio_category");
					   $project_categories = NULL;
					   
					   if ( !empty($terms) ){
					     foreach ( $terms as $term ) {
					       $project_categories .= strtolower($term->slug) . ' ';
					     }
					   }
					  
					  							
					?>
					
					<div class="col-xs-6 <?php echo 'col-sm-4' ?> element <?php echo $project_categories; ?>" data-project-cat="<?php echo $project_categories; ?>">
						

								
							<div class="portfolio_item">
								<div class="overlay"> 
                   			 	<?php 
								if(has_post_thumbnail()) {
                        			the_post_thumbnail('portfolio_medium', array('class'	=> "img-responsive"));
                            		$large_image    = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
                       			} else {
                            		echo '<img src="'.get_bloginfo("template_url").'/images/default-featured-image.png" width="200" height="200" />';
                       			}
                    			?>
								
  						<div class="icon-align two-icons">
                            <a href="<?php echo $large_image[0];?>" title="<?php the_title(); ?>" class="icon magnify"><i class="fa fa-search"></i></a>
                            <a href="<?php the_permalink(); ?>" class="icon"><i class="fa fa-link"></i></a>
                        </div><!-- .portfolio_meta -->
								</div>
								
							<div class="entry-header small-title">
								<h1 class="blog-post-title"><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h1>							
							</div>
								
								
							</div><!--portfolio_item-->
							

						

					
					</div><!--/col-->
					
				<?php endwhile; endif;wp_reset_postdata(); ?>
			</div><!--/portfolio-->
	   </div><!--/portfolio wrap-->











            
         <?php } else{ ?>  <!-- portfolio_filterable check -->
            
            
            
        
        
        
                            <?php
                    $arguments = array();
                    $arguments["post_type"] = "portfolio_item";
                    $arguments["paged"] = $paged;
                    $arguments["posts_per_page"] = get_field("num_projects");
                    $wp_query = new WP_Query( $arguments );


                   ?>
            
            <?php
               	while ( $wp_query->have_posts() ) :  $wp_query->the_post();
                	$terms = wp_get_post_terms( $post->ID,"portfolio_category" );
                    $terms_html_array = array();
                    $terms_id_array = array();
                    $term_classes = "";
                    foreach($terms as $t){
                    	$term_name = $t->name;
                        $term_link = get_term_link($t->slug,$t->taxonomy);
                        array_push($terms_html_array,"<a href='{$term_link}'>{$term_name}</a>");
                        array_push($terms_id_array,$t->slug);
                        $term_classes .= "pt_".$t->slug." ";
                    }
                    $terms_html_array = implode(", ",$terms_html_array);
            ?>
            <div class="col-sm-4 <?php if(isset($smof_plugin_data['responsive']) && $smof_plugin_data['responsive'] == 0){ echo 'col-xs-4 col-md-4';}else{echo'col-xs-6';}?>">
            	<div class="portfolio_item equalheight">
                    <div class="overlay">
                    <?php 
						if(has_post_thumbnail()) {
                        	the_post_thumbnail('portfolio_medium', array('class'	=> "img-responsive"));
                            $large_image    = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
                        } else {
                            echo '<img src="'.get_bloginfo("template_url").'/images/default-featured-image.png" width="200" height="200" />';
                        }
                    ?>
                        <div class="icon-align two-icons">
                            <a href="<?php echo $large_image[0];?>" title="<?php the_title(); ?>" class="icon magnify"><i class="fa fa-search"></i></a>
                            <a href="<?php the_permalink(); ?>" class="icon"><i class="fa fa-link"></i></a>
                        </div><!-- .portfolio_meta -->
                    </div><!-- .overlay -->
                 	<div class="entry-header small-title">
                    	<h1 class="blog-post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                    	                    		
                    		
                        	<?php if($terms_html_array):?>
                        	<div class="cat-tag">
                        	<div class="categories_list">
                        	<i class="fa fa-folder-open"></i>
                        	<?php echo $terms_html_array;?>
							</div>	
                    		</div>
                        	<?php endif; ?>
                        	
                    </div><!-- .equalheight -->
                </div><!-- .portfolio_item -->
            </div><!-- .cols -->
            <?php endwhile;wp_reset_postdata(); ?>

        
        
            
        
        
        
        
         <?php } ?>  <!-- end portfolio_filterable check -->
        
        
            
			</div>	<!--.row -->	
			<?php if(isset($smof_plugin_data['portfolio_pagination']) && $smof_plugin_data['portfolio_pagination'] == 1){?>
			<div class="btn-group-wrap">
							<?php if (function_exists("horizillax_pagination")) { horizillax_pagination();} ?>
			</div>		
			<?php } ?>

        </div> <!-- .container -->
<?php get_footer(); ?>
