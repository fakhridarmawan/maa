<?php get_header(); ?>
<div class="container">
	<div class="row">
            <?php if ( have_posts() ) : ?>
                 <div class="page-title">
                    <h1 class="main-title"><?php printf( __( 'Search Results for: %s', 'productivethemes' ), get_search_query() ); ?></h1>
                </div>
                
                <div id="primary" class="col-sm-9">
						<div id="content" class="site-content" role="main">
                <?php while ( have_posts() ) : the_post(); ?>
                	

                    <?php get_template_part( 'content' ); ?>
                    

                <?php endwhile; ?>
				</div><!-- #content -->
        </div><!-- #primary -->  
<?php get_sidebar(); ?>
                    <?php else : ?>
                <?php get_template_part( 'content', 'none' ); ?>
            <?php endif; ?>
	</div>	<!--.row -->	
			<?php  if (function_exists("horizillax_pagination")) {?>
             <div class="btn-group-wrap">
            <?php  horizillax_pagination();?>
            </div>
            <?php }?>
      </div> <!--.container -->	
<?php get_footer(); ?>