<?php get_header(); ?>
<div class="container">
	<div class="row">
				<?php if ( have_posts() ) : ?>
					<?php the_post();?>
					  <div class="page-title"><h1>
							<?php printf( __( 'All posts by %s', 'productivethemes' ), '<span class="vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '" title="' . esc_attr( get_the_author() ) . '" rel="me">' . get_the_author() . '</a></span>' ); ?>
                        </h1>

					<?php if ( get_the_author_meta( 'description' ) ) : ?>

	<div class="author-avatar">
		<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'productivethemes_author_bio_avatar_size', 74 ) ); ?>
	</div><!-- .author-avatar -->
		<h4><?php printf( __( 'About %s', 'productivethemes' ), get_the_author() ); ?></h4>
		<div class="page-desc">
			<?php the_author_meta( 'description' ); ?>
			<a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">
				<?php printf( __( 'View all posts by %s <span class="meta-nav">&rarr;</span>', 'productivethemes' ), get_the_author() ); ?>
			</a>
		</div>



					<?php endif; ?>


					</div><!-- .main-header -->
		<div id="primary" class="col-sm-9 blog-main">
			<div id="content" class="site-content" role="main">
					
					<?php rewind_posts();?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'content' ); ?>
					<?php endwhile; ?>
					<?php if (function_exists("horizillax_pagination")) { horizillax_pagination();} ?>
				<?php else : ?>
					<?php get_template_part( 'content', 'none' ); ?>
				<?php endif; ?>
			</div><!-- #content -->
		</div><!-- #primary -->
<?php get_sidebar(); ?>
	</div>	<!--.row -->	
</div> <!--.container -->	

<?php get_footer(); ?>