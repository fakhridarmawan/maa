<?php
/*Template Name:About*/
get_header(); ?>
        <div class="container about-page">
            <div class="row">
            
            <div class="page-title"><h1><?php the_title(); ?></h1>
					<?php if(get_field("heading_title")): ?>
                    	<h4><?php the_field("heading_title"); ?></h4>
                    <?php endif; ?>
                    <?php if(get_field("sub_heading")): ?>
                    	<div class="page-desc"><?php the_field("sub_heading"); ?></div>
                     <?php endif; ?>
            
            </div>
            

                <?php
					$members = get_field("team_members");
                    if($members):
                ?>
                    <?php foreach($members as $member): ?>
                        <div class="member col-sm-4 col-xs-6">
                        	<div class="member-pad equalheight">
                            	<?php if($member["member_avatar"]): ?>
                                	<div class="team-group">
                                    	<div class="team-pic">
                                        	<img class="image-height" src="<?php echo $member["member_avatar"]["sizes"]["team_member"]; ?>" alt="<?php echo $member["member_avatar"]["alt"]; ?>">
                                         </div><!-- .team-pic -->
                                         <div class="team-desc image-height">
                                          <div class="wrapper-pad"><?php echo $member["member_description"]; ?></div>
                                         </div>
                                     </div><!-- .team-pic-wrap -->
                                <?php endif; ?>
                                <div class="team-content">
                                        <h3>
										<?php echo $member["member_name"]; ?>
                                        </h3>
											 <?php if($member["member_designation"]):?>
                                            	 <p><?php echo $member["member_designation"]; ?></p>
                                             <?php endif; ?>
                                        <?php if($member["about_twitter"] || $member["about_facebook"] || $member["about_linkedin"]):?>
                                            <div class="member-social">
												<?php if($member["about_twitter"]):?><a href="<?php echo $member["about_twitter"]; ?>" class="btn btn-social-icon btn-sm btn-twitter"><i class="fa fa-twitter"></i></a><?php endif; ?>
                                                <?php if($member["about_facebook"]):?><a href="<?php echo $member["about_facebook"]; ?>" class="btn btn-social-icon btn-sm btn-facebook"><i class="fa fa-facebook"></i></a><?php endif; ?>
                                                <?php if($member["about_linkedin"]):?><a href="<?php echo $member["about_linkedin"]; ?>" class="btn btn-social-icon btn-sm btn-linkedin"><i class="fa fa-linkedin"></i></a><?php endif; ?>
                                            </div>
                                        <?php endif; ?>
                                       
                                </div><!-- .team-content -->
                       		</div><!-- .member-pad -->
                        </div><!-- .member -->
                    <?php endforeach; ?>
                </div><!-- .team -->
                <?php endif; ?>
        </div><!-- .container -->

<?php get_footer(); ?>