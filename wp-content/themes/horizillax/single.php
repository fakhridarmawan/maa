<?php global $smof_plugin_data; ?>
<?php get_header(); ?>
<div class="container">
	<div class="row">

                <?php while ( have_posts() ) : the_post(); ?>
		<div id="primary" class="<?php if(!is_dynamic_sidebar('sidebar-2 ')){echo'no-sidebar ';} 
		if(isset($smof_plugin_data['responsive']) && $smof_plugin_data['responsive'] == 0){ echo 'col-xs-9 ';}else{echo 'col-sm-9';}?> blog-main">
			<div id="content" class="site-content" role="main">
			
                    <?php get_template_part( 'content' ); ?>
                    

                    <?php comments_template(); ?>
            </div><!-- #content -->
        </div><!-- #primary -->
                <?php endwhile; ?>
<?php get_sidebar(); ?>
	</div>	<!--.row -->	
</div> <!--.container -->	
<?php get_footer(); ?>