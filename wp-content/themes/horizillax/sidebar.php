<?php global $smof_plugin_data; ?>
<?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
	<div id="tertiary" class="
	<?php if(isset($smof_plugin_data['responsive']) && $smof_plugin_data['responsive'] == 0){ echo 'col-xs-3  col-xs-offset-0';}else{echo 'col-sm-3 col-sm-offset-0';}?> blog-sidebar" role="complementary">
		<div class="sidebar-inner">
			<div class="widget-area">
				<?php dynamic_sidebar( 'sidebar-2' ); ?>
			</div><!-- .widget-area -->
		</div><!-- .sidebar-inner -->
	</div><!-- #tertiary -->
<?php endif; ?>