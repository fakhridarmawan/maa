<?php global $smof_plugin_data; ?>

<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 template-parallax" <?php language_attributes(); ?>> 
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 template-parallax" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?> class="template-parallax">
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<?php if(isset($smof_plugin_data['responsive']) && $smof_plugin_data['responsive'] == 1){?>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php }?>
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
<div class="loading-page"></div>
<div class="wrapall">
		<header id="masthead" role="banner">
 			<div class="navbar navbar-inverse" role="navigation">    
				<div class="container-fluid">
				
					<div class="navbar-header">
           				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              				<span class="sr-only">Toggle navigation</span>
             				<span class="icon-bar"></span>
              				<span class="icon-bar"></span>
              				<span class="icon-bar"></span>
            			</button>
            			<a class="navbar-brand" href="javascript:void(0);"  onclick="HORILLAX.ANIM.instance.move(0);" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
            			
            			<?php if($smof_plugin_data['header_logo']):?>
            			<?php if (strpos($smof_plugin_data['header_logo'],'[theme-link]') !== false) {
						$logourl = str_replace('[theme-link]', get_stylesheet_directory_uri(), $smof_plugin_data['header_logo']);
						}
						else{
						$logourl = $smof_plugin_data['header_logo'];
						}?>
							<img src="<?php echo $logourl; ?>"  alt="<?php bloginfo("name"); ?>"/>
    					<?php else: ?>
							<h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
							<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
           				<?php endif; ?>  
            			
            			</a>
         			</div>
         			
         			<nav role="navigation" class="collapse navbar-collapse" id="main-navbar">
                          							
<?php
							
							



		function recursiveParallaxMenu($p_id){
			$children = get_pages("child_of=$p_id&sort_column=menu_order");
			$immediate_children = get_pages("child_of=$p_id&parent=$p_id&sort_column=menu_order");
			if($children) {
				echo '<li class="menu-item dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">'.get_the_title($p_id).'</a><ul class="dropdown-menu" role="menu">';
				foreach($immediate_children as $child) {
					recursiveParallaxMenu($child->ID);
				}
				echo '</ul></li>';    
			}
			else {
				echo '<li class="menu-item"><a href="javascript:void(0);">'.get_the_title($p_id).'</a></li>';
			}
  
		}


                            
	if ( has_nav_menu('primary') ) {
		wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav navbar-nav no-float', 'walker'=>new horizillax_bootstrap_navwalker_plx ) );
	} 
	else{
								
		echo '<ul class="nav navbar-nav no-float" id="parallax-menu">';
		$top_level_pages = get_pages('parent=0&sort_column=menu_order&post_type=page&post_status=publish');
			
			foreach($top_level_pages as $page) {
    			$p_id = $page->ID;
    			recursiveParallaxMenu($p_id);
			}
		echo '</ul>';	

	}

	?>
							
							
			<div id="social-icons" class="pull-right">
			<?php if(isset($smof_plugin_data['soc_twitter']) && $smof_plugin_data['soc_twitter']==1 && isset($smof_plugin_data['twitter_url'])){ 
				echo '<a href="'.$smof_plugin_data['twitter_url'].'" class="btn btn-social-icon btn-sm btn-twitter"><i class="fa fa-twitter"></i></a>'; 
			}?>
			<?php if(isset($smof_plugin_data['soc_facebook']) && $smof_plugin_data['soc_facebook']==1 && isset($smof_plugin_data['facebook_url'])){ 
				echo '<a href="'.$smof_plugin_data['facebook_url'].'" class="btn btn-social-icon btn-sm btn-facebook"><i class="fa fa-facebook"></i></a>'; 
			}?>
			<?php if(isset($smof_plugin_data['soc_youtube']) && $smof_plugin_data['soc_youtube']==1 && isset($smof_plugin_data['youtube_url'])){ 
				echo '<a href="'.$smof_plugin_data['youtube_url'].'" class="btn btn-social-icon btn-sm btn-youtube"><i class="fa fa-youtube"></i></a>'; 
			}?>	
			<?php if(isset($smof_plugin_data['soc_linkedin']) && $smof_plugin_data['soc_linkedin']==1 && isset($smof_plugin_data['linkedin_url'])){ 
				echo '<a href="'.$smof_plugin_data['linkedin_url'].'" class="btn btn-social-icon btn-sm btn-linkedin"><i class="fa fa-linkedin"></i></a>'; 
			}?>
			<?php if(isset($smof_plugin_data['soc_flickr']) && $smof_plugin_data['soc_flickr']==1 && isset($smof_plugin_data['flickr_url'])){ 
				echo '<a href="'.$smof_plugin_data['flickr_url'].'" class="btn btn-social-icon btn-sm btn-flickr"><i class="fa fa-flickr"></i></a>'; 
			}?>			
			<?php if(isset($smof_plugin_data['soc_pinterest']) && $smof_plugin_data['soc_pinterest']==1 && isset($smof_plugin_data['pinterest_url'])){ 
				echo '<a href="'.$smof_plugin_data['pinterest_url'].'" class="btn btn-social-icon btn-sm btn-pinterest"><i class="fa fa-pinterest"></i></a>'; 
			}?>		
			<?php if(isset($smof_plugin_data['soc_googleplus']) && $smof_plugin_data['soc_googleplus']==1 && isset($smof_plugin_data['googleplus_url'])){ 
				echo '<a href="'.$smof_plugin_data['googleplus_url'].'" class="btn btn-social-icon btn-sm btn-google-plus"><i class="fa fa-google-plus"></i></a>'; 
			}?>
			<?php if(isset($smof_plugin_data['soc_dribbble']) && $smof_plugin_data['soc_dribbble']==1 && isset($smof_plugin_data['dribbble_url'])){ 
				echo '<a href="'.$smof_plugin_data['dribbble_url'].'" class="btn btn-social-icon btn-sm btn-dribbble"><i class="fa fa-dribbble"></i></a>'; 
			}?>					
			<?php if(isset($smof_plugin_data['soc_vimeo']) && $smof_plugin_data['soc_vimeo']==1 && isset($smof_plugin_data['vimeo_url'])){ 
				echo '<a href="'.$smof_plugin_data['vimeo_url'].'" class="btn btn-social-icon btn-sm btn-vimeo"><i class="fa fa-vimeo-square"></i></a>'; 
			}?>
			<?php if(isset($smof_plugin_data['soc_tumblr']) && $smof_plugin_data['soc_tumblr']==1 && isset($smof_plugin_data['tumblr_url'])){ 
				echo '<a href="'.$smof_plugin_data['tumblr_url'].'" class="btn btn-social-icon btn-sm btn-tumblr"><i class="fa fa-tumblr"></i></a>'; 
			}?>		                
			<?php if(isset($smof_plugin_data['soc_instagram']) && $smof_plugin_data['soc_instagram']==1 && isset($smof_plugin_data['instagram_url'])){ 
				echo '<a href="'.$smof_plugin_data['instagram_url'].'" class="btn btn-social-icon btn-sm btn-instagram"><i class="fa fa-instagram"></i></a>'; 
			}?>
			<?php if(isset($smof_plugin_data['soc_github']) && $smof_plugin_data['soc_github']==1 && isset($smof_plugin_data['github_url'])){ 
				echo '<a href="'.$smof_plugin_data['github_url'].'" class="btn btn-social-icon btn-sm btn-github"><i class="fa fa-github"></i></a>'; 
			}?>	
            </div>
							
							
						</nav>


		


				</div><!--/.container-fluid -->
			</div><!-- .navbar -->
		</header><!-- #masthead -->
