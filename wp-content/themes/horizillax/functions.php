<?php 
/**
 * Horizillax functions and definitions.
 */
 

#-----------------------------------------------------------------#
# Function to get the page link back to all portfolio items
#-----------------------------------------------------------------#

function get_portfolio_list_link($post_id) {
    global $wpdb;
	
    $results = $wpdb->get_results("SELECT post_id FROM $wpdb->postmeta
    WHERE meta_key='_wp_page_template' AND meta_value='template-portfolio.php'");
    
	//safety net
    $page_id = null;
	 
    foreach ($results as $result) 
    {
        $page_id = $result->post_id;
    }
	
    return get_page_link($page_id);
} 


/**
 * Add Favicon
 * ----------------------------------------------------------------------------
 */ 
 
function horizillax_favicon() {
	$smof_theme_data = of_get_options();
	if(isset($smof_theme_data['favicon']) && $smof_theme_data['favicon'] !== ''){
	
	if (strpos($smof_theme_data['favicon'],'[theme-link]') !== false) {
		$faviconurl = str_replace('[theme-link]', get_stylesheet_directory_uri(), $smof_theme_data['favicon']);
	}
	else{
		$faviconurl = $smof_theme_data['favicon'];
	}
		echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.$faviconurl.'" />';
	}
}
add_action('wp_head', 'horizillax_favicon');

/**
 * Check if a template is active
 * ----------------------------------------------------------------------------
 */  
function is_pagetemplate_active($pagetemplate = '') {
	global $wpdb;
	$sql = "select meta_key from $wpdb->postmeta where meta_key like '_wp_page_template' and meta_value like '" . $pagetemplate . "'";
	$result = $wpdb->query($sql);
	if ($result) {
		return TRUE;
	} else {
		return FALSE;
	}
}


#-----------------------------------------------------------------#
# Category walker for portfolio filter
#-----------------------------------------------------------------# 

class Walker_Portfolio_Filter extends Walker_Category {
   function start_el(&$output, $category, $depth = 0, $args = array(), $current_object_id = 0) {

      extract($args);
      $cat_slug = esc_attr( $category->slug );
      $cat_slug = apply_filters( 'list_cats', $cat_slug, $category );
	  
      $link = '<li><a href="#" data-original-title="'.$category->count.'" data-filter=".'.strtolower(preg_replace('/\s+/', '-', $cat_slug)).'">';
	  
	  $cat_name = esc_attr( $category->name );
      $cat_name = apply_filters( 'list_cats', $cat_name, $category );
      
      
	  	
      $link .= $cat_name;
	  
      if(!empty($category->description)) {
         $link .= ' <span>'.$category->description.'</span>';
      }
	  
      $link .= '</a>';
     
      $output .= $link;
       
   }
}



/**
 * Custom Class for bootstrap
 * ----------------------------------------------------------------------------
 */ 
function horizillax_widget_custom_class($params) {

	global $horizillax_widget_num;
	$this_id = $params[0]['id']; 
	$arr_registered_widgets = wp_get_sidebars_widgets();

	if(!$horizillax_widget_num) {
		$horizillax_widget_num = array();
	}

	if(!isset($arr_registered_widgets[$this_id]) || !is_array($arr_registered_widgets[$this_id])) {
		return $params;
	}

	$class = 'class="sidebar-module ';
	$params[0]['before_widget'] = str_replace('class="', $class, $params[0]['before_widget']);
	return $params;

}

add_filter('dynamic_sidebar_params','horizillax_widget_custom_class');


/**
 * Add bootstrap Classes top wp_page_menu
 * ----------------------------------------------------------------------------
 */ 
function horizillax_add_menuid ($page_markup) {
	preg_match('/^<div class=\"([a-z0-9-_]+)\">/i', $page_markup, $matches);
	$divclass = $matches[1];
	$toreplace = array('<div class="'.$divclass.'">', '</div>');
	$new_markup = str_replace($toreplace, '', $page_markup);
	$new_markup = preg_replace('/^<ul>/i', '<ul id="main-nav" class="nav navbar-nav no-float">', $new_markup);
	return $new_markup; 
}

add_filter('wp_page_menu', 'horizillax_add_menuid');
 
 
/**
 * Function to set excerpt limit
 * ----------------------------------------------------------------------------
 */  
function excerpt($limit) {

	if(get_the_excerpt()){
		$excerpt = explode(' ', get_the_excerpt(), $limit);
		
		if (count($excerpt)>=$limit) {
			array_pop($excerpt);
			$excerpt = implode(" ",$excerpt).'…';
		} else {
			$excerpt = implode(" ",$excerpt);
		}	
		
		$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
		return $excerpt;
	}
	else{
		$content = explode(' ', get_the_content(), $limit);
		
		if (count($content)>=$limit) {
			array_pop($content);
			$content = implode(" ",$content).'…';
		} else {
			$content = implode(" ",$content);
		}
			
		$content = preg_replace('/\[.+\]/','', $content);
		$content = apply_filters('the_content', $content);
		$content = str_replace(']]>', ']]&gt;', $content);
		return $content;
	}

}


/**
 * Add Image Thumbnail Sizes
 * ----------------------------------------------------------------------------
 */ 
 if ( function_exists( 'add_image_size' ) ) { 
 	add_image_size("team_member",350,350,true);
	add_image_size("portfolio_thumb",300,300,true);
	add_image_size("portfolio_medium",450,450,true);
	add_image_size("portfolio_large",960,450,true);
	add_image_size("blog_medium",960,350,true);
	add_image_size("blog_parallax",520,310,true);
	}	
	
/**
 * Add bootstrap classes to nav menu
 * ----------------------------------------------------------------------------
 */
 	require_once ('horizillax-bootstrap-navwalker.php');
 	require_once ('horizillax-bootstrap-navwalker-plx.php');

 	
 	
/**
 * Set up Theme Options Custom Post Types and Custom Widgets
 * ----------------------------------------------------------------------------
 */ 	 
	require_once ('admin/index.php');
	require_once "inc/widgets/widgets.php";
	require_once ('inc/theme-post-types.php');
	add_action( 'init', 'convax_post_types' );
	$smof_theme_data = of_get_options();



/**
 * Set up Custom Fields
 * ----------------------------------------------------------------------------
 */
	define( 'ACF_LITE' , true );
	include_once('acf/acf.php' );
	require_once "inc/custom-fields.php";
	add_action('acf/register_fields', 'register_fields');
	
	function register_fields(){
    	include_once('inc/acf-location-field/acf-location.php');
	}


/**
 * Upgrade notice if using older version of Wordpress
 * ----------------------------------------------------------------------------
 */ 	
	if ( version_compare( $GLOBALS['wp_version'], '3.6-alpha', '<' ) )
		require get_template_directory() . '/inc/back-compat.php';


 /**
 * Sets up theme defaults & registers the various WordPress features supported.
 * ----------------------------------------------------------------------------
 */ 
	function horizillax_setup() {
	
		// This theme styles the visual editor to resemble the theme style,
		add_editor_style( array( 'css/editor-style.css', horizillax_fonts_url() ) );

		// Adds RSS feed links to <head> for posts and comments.
		add_theme_support( 'automatic-feed-links' );

		// Switches default core markup for search form, comment form, and comments
		// to output valid HTML5.
		add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

		/*
	 	* This theme supports all available post formats by default.
	 	*/
		add_theme_support( 'post-formats', array(
			'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video'
		) );
		// This theme uses wp_nav_menu() in one location.
		register_nav_menu( 'primary', __( 'Primary Menu', 'productivethemes' ) );
		
		add_theme_support( 'custom-header');
		add_theme_support( 'custom-background');

		/*
 		* This theme uses a custom image size for featured images, displayed on
 		* "standard" posts and pages.
 		*/
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 604, 270, true );

		// This theme uses its own gallery styles.
		add_filter( 'use_default_gallery_style', '__return_false' );
	}
	add_action( 'after_setup_theme', 'horizillax_setup' );
	


/**
 * Returns the Google font stylesheet URL, if available.
 * ----------------------------------------------------------------------------
 */ 

	function horizillax_fonts_url() {
		$fonts_url = '';
		/* Translators: If there are characters in your language that are not
		 * supported by custom font, translate this to 'off'. Do not translate
		 * into your own language.
	 	*/
		$lato = _x( 'on', 'Lato font: on or off', 'productivethemes' );
		$opensans = _x( 'on', 'OpenSans font: on or off', 'productivethemes' );
		$montsterrat = _x( 'on', 'Montsterrat font: on or off', 'productivethemes' );
		$raleway = _x( 'on', 'Raleway font: on or off', 'productivethemes' );

		if ( 'off' !== $raleway |  'off' !== $lato | 'off' !== $opensans | 'off' !== $montsterrat) {
			$font_families = array();

			if ( 'off' !== $raleway )
				$font_families[] = 'Raleway:400,500,600,700';
			
			if ( 'off' !== $lato )
				$font_families[] = 'Lato:400,700';
				
			if ( 'off' !== $opensans )
				$font_families[] = 'Open+Sans:400,700';
			
			if ( 'off' !== $montsterrat )
				$font_families[] = 'Montserrat:400,700';
							
			$query_args = array(
				'family' => urlencode( implode( '|', $font_families ) ),
				'subset' => urlencode( 'latin,latin-ext' ),
			);
			$fonts_url = add_query_arg( $query_args, "//fonts.googleapis.com/css" );
		}
		return $fonts_url;
	}
	

/**
 * Enqueues scripts and styles for front end.
 * ----------------------------------------------------------------------------
 */ 	

	function horizillax_scripts_styles() {
		
		$smof_theme_data = of_get_options();
						
		// Adds JavaScript to pages with the comment form to support sites with
		// threaded comments (when in use).
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );
		
						
		// Set equal heights to sidebar and content area
		wp_enqueue_script( 'equal-height-columns', get_template_directory_uri().'/js/jquery.equalheightcolumns.js', array(), '1.1', true );
		
		global $is_IE;
		if($is_IE){
		// Mordernizr
		wp_enqueue_script( 'modernizr', get_template_directory_uri().'/js/modernizr.js', array(), '2.7.1', false );	
		wp_enqueue_script( 'respond', get_template_directory_uri().'/js/respond.js', array(), '1.1', false );	
		}	

		wp_enqueue_script( 'bx-script', get_template_directory_uri().'/js/jquery.bxslider.min.js', array( 'jquery' ), '2014-02-23', true );
		
		wp_enqueue_script( 'magnificjs', get_template_directory_uri().'/js/jquery.magnific-popup.min.js', array( 'jquery' ), '2014-03-12', true );

		//Load bootstrap
		wp_enqueue_script( 'bootstrap',get_template_directory_uri().'/js/bootstrap.min.js',array(),3.0,false);

		
		

		
		if ( is_page_template('template-parallax.php') ){
		
			if(isset($smof_theme_data['mousewheel_parallax']) && $smof_theme_data['mousewheel_parallax'] == 1){
			wp_enqueue_script( 'mousewheel', get_template_directory_uri().'/js/jquery.mousewheel.min.js', array( 'jquery' ), '2014-22-04', true );
			wp_enqueue_script( 'parallax_mousewheel', get_template_directory_uri().'/js/parallax.mousewheel.js', array( 'jquery' ), '2014-22-04', true );
			}
			
			if(isset($smof_theme_data['swipe_parallax']) && $smof_theme_data['swipe_parallax'] == 1){
			wp_enqueue_script( 'touchswipe', get_template_directory_uri().'/js/jquery.touchSwipe.min.js', array( 'jquery' ), '2014-23-04', true );
			wp_enqueue_script( 'parallax_swipe', get_template_directory_uri().'/js/parallax.swipe.js', array( 'jquery' ), '2014-23-04', true );
			}
			
			if(isset($smof_theme_data['keyboard_parallax']) && $smof_theme_data['keyboard_parallax'] == 1){
			wp_enqueue_script( 'parallax_keypress', get_template_directory_uri().'/js/parallax.keypress.js', array( 'jquery' ), '2014-23-04', true );
			}			
						
			if(isset($smof_theme_data['show_preloader']) && $smof_theme_data['show_preloader'] == 1){
			wp_enqueue_script( 'preloaderjs',get_template_directory_uri().'/js/pace.min.js',array(),1.0,false);
			wp_enqueue_style( 'preloadercss', get_template_directory_uri().'/css/pace-theme-center-simple.css', array(), '1.0' );
			}
			
			wp_enqueue_script( 'enhancedanimation',get_template_directory_uri().'/js/jquery.animate-enhanced.min.js',array(),1.0,false);
			wp_enqueue_script( 'parallax',get_template_directory_uri().'/js/parallax.js',array(),1.0,true);
			wp_enqueue_script( 'nanoscrollerjs',get_template_directory_uri().'/js/jquery.nanoscroller.min.js',array(),1.0,false);
			wp_enqueue_style( 'nanoscroller', get_template_directory_uri().'/css/nanoscroller.css', array(), '1.0' );
			wp_enqueue_script( 'parallaxtemplate', get_template_directory_uri().'/js/parallax.functions.js',array(),1.0,true);
			
		}
		else{
			// All theme specific functions
			wp_enqueue_script( 'productivethemes-script', get_template_directory_uri().'/js/page.functions.js', array( 'jquery' ), '2013-10-17', true );
		}

		
		if ( is_page_template('template-portfolio.php') && isset($smof_theme_data['portfolio_filterable']) && $smof_theme_data['portfolio_filterable'] == 1 ){
			wp_enqueue_script( 'imagesloaded', get_template_directory_uri().'/js/imagesloaded.pkgd.min.js', array( 'jquery' ), '2014-28-04', false );
			wp_enqueue_script( 'isotope', get_template_directory_uri().'/js/isotope.js', array( 'jquery' ), '2014-25-04', false );
			wp_enqueue_script( 'init', get_template_directory_uri().'/js/portfolio.filter.js', array( 'jquery' ), '2014-25-04', false );
		
		}
		

		// Loads all stylesheets.
		// Add Google Fonts used in the main stylesheet.
		wp_enqueue_style( 'productivethemes-fonts', horizillax_fonts_url(), array(), null );
		
		wp_enqueue_style( 'bootstrapcss', get_template_directory_uri().'/css/bootstrap.min.css', array(), '1.0' );
				
		wp_enqueue_style( 'horizillax-style', get_stylesheet_uri(), array(), '2013-07-18' );
		
		if(isset($smof_theme_data['responsive']) && $smof_theme_data['responsive'] == 0){
			wp_enqueue_style( 'nonresponsive', get_template_directory_uri().'/css/non-responsive.css', array(), '1.0' );
		}
		else{
			wp_enqueue_style( 'responsive', get_template_directory_uri().'/css/responsive.css', array(), '1.0' );
		}
		
		wp_enqueue_style( 'magnificss', get_template_directory_uri().'/css/magnific-popup.css', array(), '1.0' );
		
		// Add the user's color choice.
		if(isset($smof_theme_data["colorstyles"])){$styles = $smof_theme_data["colorstyles"];}else{$styles = "grey.css";};
		wp_enqueue_style( 'chosen', get_template_directory_uri().'/css/colors/'.$styles, array(), '1.0' );
		
		// Get variables from theme options
 		
		if( $smof_theme_data['convax_body_bg'] == (get_template_directory_uri().'/images/bg/bgnone.png')){
		$bodybg ="";
		}
		else{
		$bodybg = $smof_theme_data['convax_body_bg'];
		}
		
		$custom_css = "";
		// Add the custom styles.
		if($smof_theme_data["max_width"]){
			$custom_css.= "
			.container-fluid, .container{
				max-width:{$smof_theme_data["max_width"]}px;";
			$custom_css.="}";
		}



		if(isset($smof_theme_data['body_font'])){
			$custom_css.= "
			html,body{
				font:{$smof_theme_data['body_font']['style']} {$smof_theme_data['body_font']['size']}/{$smof_theme_data['body_font']['height']} ";
				if($smof_theme_data['body_google_font'] && $smof_theme_data['body_google_font']!=="none"){
					$custom_css.= "{$smof_theme_data['body_google_font']},";
				}
				$custom_css.= " {$smof_theme_data['body_font']['face']};";
				if($smof_theme_data['body_font']['color']){
					$custom_css.= " color:{$smof_theme_data['body_font']['color']};";
				}
				if($bodybg){
					$custom_css.= "background-image: url({$bodybg});";
				}
				
			$custom_css.="}";
		}
		
		if(isset($smof_theme_data['responsive']) && !$smof_theme_data['responsive'] == 1){
			$custom_css.=".container-fluid, .container{width:{$smof_theme_data["max_width"]}px;}";
			$custom_css.= "html,body{display:table;}";
			$custom_css.= ".template-parallax, .template-parallax body{display:block;}";
		}
		
		
		
		if(isset($smof_theme_data['title_font'])){
			$custom_css.= "
			.page-title h1,
			.parallax-page .page-title h1,
			.page .page-title h1,
			.author .page-title h1{
				font:{$smof_theme_data['title_font']['style']} {$smof_theme_data['title_font']['size']}/{$smof_theme_data['title_font']['height']} ";
				if($smof_theme_data['title_google_font'] && $smof_theme_data['title_google_font']!=="none"){
					$custom_css.= "{$smof_theme_data['title_google_font']},";
				}
				$custom_css.= " {$smof_theme_data['title_font']['face']};";
				if($smof_theme_data['title_font']['color']){
					$custom_css.= " color:{$smof_theme_data['title_font']['color']};";
				}
				if(isset($smof_theme_data['title_capitalize']) && $smof_theme_data['title_capitalize'] == 1){
					$custom_css.= "text-transform:uppercase;";
				}
			$custom_css.="}";
		}		
		
		
		
		if(isset($smof_theme_data['subtitle_font'])){
			$custom_css.= "
			.page-title h4,
			.parallax-page .page-title h4,
			.page .page-title h4,
			.author .page-title h4{
				font:{$smof_theme_data['subtitle_font']['style']} {$smof_theme_data['subtitle_font']['size']}/{$smof_theme_data['subtitle_font']['height']} ";
				if($smof_theme_data['subtitle_google_font'] && $smof_theme_data['subtitle_google_font']!=="none"){
					$custom_css.= "{$smof_theme_data['subtitle_google_font']},";
				}
				$custom_css.= " {$smof_theme_data['subtitle_font']['face']};";
				if($smof_theme_data['subtitle_font']['color']){
					$custom_css.= " color:{$smof_theme_data['subtitle_font']['color']};";
				}
				if(isset($smof_theme_data['subtitle_capitalize']) && $smof_theme_data['subtitle_capitalize'] == 1){
					$custom_css.= "text-transform:uppercase;";
				}
			$custom_css.="}";
		}		


		if(isset($smof_theme_data['menu_font'])){
			$custom_css.= "
			.navbar-inverse .navbar-nav > li > a{
				font:{$smof_theme_data['menu_font']['style']} {$smof_theme_data['menu_font']['size']}/{$smof_theme_data['menu_font']['height']} ";
				if($smof_theme_data['menu_google_font'] && $smof_theme_data['menu_google_font']!=="none"){
					$custom_css.= "{$smof_theme_data['menu_google_font']},";
				}
				$custom_css.= " {$smof_theme_data['menu_font']['face']};";
				if($smof_theme_data['menu_font']['color']){
					$custom_css.= " color:{$smof_theme_data['menu_font']['color']};";
				}
				if(isset($smof_theme_data['menu_capitalize']) && $smof_theme_data['menu_capitalize'] == 1){
					$custom_css.= "text-transform:uppercase;";
				}
			$custom_css.="}";
		}	

		if(isset($smof_theme_data['widget_font'])){
			$custom_css.= "
			.widget-title{
				font:{$smof_theme_data['widget_font']['style']} {$smof_theme_data['widget_font']['size']}/{$smof_theme_data['widget_font']['height']} ";
				if($smof_theme_data['widget_google_font'] && $smof_theme_data['widget_google_font']!=="none"){
					$custom_css.= "{$smof_theme_data['widget_google_font']},";
				}
				$custom_css.= " {$smof_theme_data['widget_font']['face']};";
				if($smof_theme_data['widget_font']['color']){
					$custom_css.= " color:{$smof_theme_data['widget_font']['color']};";
				}
				if(isset($smof_theme_data['widget_capitalize']) && $smof_theme_data['widget_capitalize'] == 1){
					$custom_css.= "text-transform:uppercase;";
				}
			$custom_css.="}";
		}			
		

		if(isset($smof_theme_data['headings_font'])){
			$custom_css.= "
			.blog-post-title{
				font:";
				if($smof_theme_data['headings_font']['style'] == 'bold' && $smof_theme_data['headings_google_font']!=="none" ){
				$custom_css.= "600";
				}
				else{
				$custom_css.= "{$smof_theme_data['headings_font']['style']}";
				}
				
				$custom_css.= " {$smof_theme_data['headings_font']['size']}/{$smof_theme_data['headings_font']['height']} ";
				if($smof_theme_data['headings_google_font'] && $smof_theme_data['headings_google_font']!=="none"){
					$custom_css.= "{$smof_theme_data['headings_google_font']},";
				}
				$custom_css.= " {$smof_theme_data['headings_font']['face']};";
				if($smof_theme_data['headings_font']['color']){
					$custom_css.= " color:{$smof_theme_data['headings_font']['color']};";
				}
				if(isset($smof_theme_data['blogpost_capitalize']) && $smof_theme_data['blogpost_capitalize'] == 1){
					$custom_css.= "text-transform:uppercase;";
				}
			$custom_css.="}";
		}
			
		
		if($smof_theme_data['headings_font']['color'] || $smof_theme_data['headings_google_font']){
			$custom_css.= " h1,h2,h3,h4,h5,h6{
			color:{$smof_theme_data['headings_font']['color']};";
			if($smof_theme_data['headings_google_font']){
			$custom_css.= "font-family:{$smof_theme_data['headings_google_font']};";
			}
			else{
			$custom_css.= "font-family:{$smof_theme_data['headings_font']};";
			}
			
			if($smof_theme_data['headings_font']['style'] == 'bold' && $smof_theme_data['headings_google_font']!=="none" ){
				$custom_css.= "font-weight:500;";
			}
			else{
				$custom_css.= "font-weight:{$smof_theme_data['headings_font']['style']};";
			}
			
			$custom_css.= "}";
		}
		
		
		if($smof_theme_data['headings_font']['color']){
			$custom_css.= "
			.parallax-page .entry-header .blog-post-title, 
			.parallax-page .entry-header .blog-post-title a{";
			
			if($smof_theme_data['headings_font']['color']){
				$custom_css.= " color:{$smof_theme_data['headings_font']['color']};";
			}
			
			$custom_css.="}";
			}

				
		if($smof_theme_data["body_background"]){
		$custom_css.= "
			html,body{
				background-color:".$smof_theme_data["body_background"].";
			}";
		
		}

		
		if(isset($smof_theme_data["blog_text_color"]) && !empty($smof_theme_data["blog_text_color"])){
		$custom_css.= "
			.page .entry-content,
			.comments-area,
			.comment-respond,
			.team-desc,
			.blog-main,
			.blog-sidebar{
				color:".$smof_theme_data["blog_text_color"].";
			}";
		}		


		if($smof_theme_data["input_color"] && $smof_theme_data["input_bg_color"]){
		$custom_css.= "
			.search-field,
			#commentform #author, 
			#commentform #email, 
			#commentform #url, 
			#commentform textarea,
			textarea,
			input[type='text'],
			.search-form .search-submit,
			.widget .btn-default.search-submit i,
			.search-form .search-submit:hover, 
			.search-form .search-submit:hover i{
				color:".$smof_theme_data["input_color"].";
				background:".$smof_theme_data["input_bg_color"].";
			}";
		}
				
		if($smof_theme_data["header_bg_color"] && $smof_theme_data["header_bg_color"]){
		$custom_css.= "
			.navbar-inverse,
			.pagination > li > a, 
			.pagination > li > span,
			.pagination > li > a:hover, 
			.pagination > li > span:hover, 
			.pagination > li > a:focus, 
			.pagination > li > span:focus,
			#portfolio-cat-filter{
			background:".$smof_theme_data["header_bg_color"].";
			}";
		}	

		if($smof_theme_data["menu_link_color"] && $smof_theme_data["menu_link_color"]){
		$custom_css.= "
			.navbar-inverse .navbar-nav > li > a,
			.pagination > li a,
			.pagination > li > span:hover, .pagination > li > a:focus, .pagination > li > span:focus{
			color:".$smof_theme_data["menu_link_color"].";
			}";
		}
		
		if($smof_theme_data["menu_link_color_hover"] && $smof_theme_data["menu_link_color_hover"]){
		$custom_css.= "
			.navbar-inverse .navbar-nav > li > a:hover,
			.pagination > li a:hover{
			color:".$smof_theme_data["menu_link_color_hover"].";
			}";
		}

		if($smof_theme_data["active_link_color"] && $smof_theme_data["active_link_color"]){
		$custom_css.= "
			.navbar-inverse .navbar-nav > .current_page_item > a.dropdown-toggle, 
			nav .current_page_item, nav .current_page_item:hover, 
			nav .current_page_item a:focus, 
			.navbar-inverse .navbar-nav > li.current_page_item > a:hover, 
			.navbar-inverse .navbar-nav > li.current_page_item > a:focus, 
			nav .dropdown-menu > .active > a, 
			nav .dropdown-menu > .active > a:hover, 
			nav .dropdown-menu > .active > a:focus, 
			#masthead .navbar-nav li.current-menu-item, 
			.navbar-inverse .navbar-nav > .current_page_item > a, 
			.navbar-inverse .navbar-nav > .current_page_item > a:hover, 
			.navbar-inverse .navbar-nav > .current_page_item > a:focus,
			.pagination > li.active a,
			.pagination > .disabled > span, 
			.pagination > .disabled > span:hover, 
			.pagination > .disabled > span:focus, 
			.pagination > .disabled > a, 
			.pagination > .disabled > a:hover, 
			.pagination > .disabled > a:focus{
				color:".$smof_theme_data["active_link_color"]." !important;
			}";
		}


		if($smof_theme_data["active_link_bg"] && $smof_theme_data["active_link_bg"]){
		$custom_css.= "
			.navbar-inverse .navbar-nav > .current_page_item > a.dropdown-toggle, 
			nav .current_page_item, nav .current_page_item:hover, 
			nav .current_page_item a:focus, 
			.navbar-inverse .navbar-nav > li.current_page_item > a:hover, 
			.navbar-inverse .navbar-nav > li.current_page_item > a:focus, 
			nav .dropdown-menu > .active > a, 
			nav .dropdown-menu > .active > a:hover, 
			nav .dropdown-menu > .active > a:focus, 
			#masthead .navbar-nav li.current-menu-item, 
			.navbar-inverse .navbar-nav > .current_page_item > a, 
			.navbar-inverse .navbar-nav > .current_page_item > a:hover, 
			.navbar-inverse .navbar-nav > .current_page_item > a:focus,
			.pagination > li.active a,
			.pagination > .active > a, 
			.pagination > .active > span, 
			.pagination > .active > a:hover, 
			.pagination > .active > span:hover, 
			.pagination > .active > a:focus, 
			.pagination > .active > span:focus{
				background:".$smof_theme_data["active_link_bg"].";
			}";
		}
		
				
		if(isset($smof_theme_data["blog_link_color"]) && !empty($smof_theme_data["blog_link_color"])){
		$custom_css.= "
			.blog-main a,
			.blog-sidebar a{
				color:".$smof_theme_data["blog_link_color"].";
			}";
		}
		
		if(isset($smof_theme_data["blog_link_hover_color"]) && !empty($smof_theme_data["blog_link_hover_color"])){
		$custom_css.= "
			.blog-main a:hover,
			.blog-sidebar a:hover{
				color:".$smof_theme_data["blog_link_hover_color"].";
			}";
		}
		
				
		if($smof_theme_data["link_color"]){
		$custom_css.= "
			a{
				color:".$smof_theme_data["link_color"].";
			}";
		}
		
		if($smof_theme_data["link_hover_color"]){
		$custom_css.= "
			a:hover{
				color:".$smof_theme_data["link_hover_color"].";
			}";
		}


		if(isset($smof_theme_data["parallax_link_color"]) && !empty($smof_theme_data["parallax_link_color"])){
		$custom_css.= "
			.parallax-page a{
				color:".$smof_theme_data["parallax_link_color"].";
			}";
		}
		
		if(isset($smof_theme_data["parallax_hover_color"]) && !empty($smof_theme_data["parallax_hover_color"])){
		$custom_css.= "
			.parallax-page a:hover{
				color:".$smof_theme_data["parallax_hover_color"].";
			}";
		}
		
		
		if(isset($smof_theme_data["contentbox_link_color"]) && !empty($smof_theme_data["contentbox_link_color"])){
		$custom_css.= "
			.parallax-page .content-box a{
				color:".$smof_theme_data["contentbox_link_color"].";
			}";
		}
		
		if(isset($smof_theme_data["contentbox_hover_color"]) && !empty($smof_theme_data["contentbox_hover_color"])){
		$custom_css.= "
			.parallax-page .content-box  a:hover{
				color:".$smof_theme_data["contentbox_hover_color"].";
			}";
		}
		
				
		if(isset($smof_theme_data["parallax_text_color"]) && !empty($smof_theme_data["parallax_text_color"])){
		$custom_css.= "
			.parallax-page, .template-parallax, .parallax-page .page-title h1,.parallax-page .page-title h4{
				color:".$smof_theme_data["parallax_text_color"].";
			}";
		
		}	
				

		if(isset($smof_theme_data["blog_bg_color"]) && !empty($smof_theme_data["blog_bg_color"])){
		$custom_css.= "
			.blog-post, 
			.sidebar-module, 
			.widget{
				background:".$smof_theme_data["blog_bg_color"].";
			}";
		
		}	
					
		if(isset($smof_theme_data["content_stack"]) && $smof_theme_data['content_stack'] == 1){
			$custom_css.="
			.parallax-page:hover{
				z-index: 999 !important;
				border:none;
			}";
		}
		
		if($smof_theme_data["button_color"] && $smof_theme_data["button_text_color"]){
			$custom_css.="
			.btn-default, 
			.parallax-page .content-box a.btn-default,
			.parallax-page .content-box a.btn-default i,
			.btn-group .btn-default i,
			.widget .btn-default i,
			.widget_tag_cloud a,
			.widget  tfoot a,
			.icon{
				background:".$smof_theme_data["button_color"].";
				color:".$smof_theme_data["button_text_color"].";
				border:none;
			}";
		}

		if($smof_theme_data["button_color_hover"] && $smof_theme_data["button_text_color_hover"]){
			$custom_css.="
			.parallax-page .content-box a.btn-default:hover i,
			.parallax-page .content-box a.btn-default:hover,
			.btn-default:hover, 
			.content-box .btn-default:hover,
			.btn-default:hover i,
			.widget_tag_cloud a:hover,
			.btn-default:focus, 
			.btn-default:active, 
			.btn-default:focus i,
			.btn-default:active i,
			.btn-default.active i, 
			.btn-default.active, 
			.open .dropdown-toggle.btn-default,
			.icon:hover, 
			.icon:hover i,
			.parallax-page .btn:hover,
			.widget  tfoot a:hover{
				background:".$smof_theme_data["button_color_hover"].";
				color:".$smof_theme_data["button_text_color_hover"].";
				border:none;
			}";
		}	

		
		if($smof_theme_data['show_preloader'] && $smof_theme_data['show_preloader'] == 1){
			if($smof_theme_data["preloader_bar_color"] && $smof_theme_data["preloader_progress_bar_color"] && $smof_theme_data["preloader_text_color"] && $smof_theme_data["preloader_bg_color"]){
				$custom_css.="
				.pace{
					background:".$smof_theme_data["preloader_bar_color"].";
				}
				.pace .pace-progress{
					background:".$smof_theme_data["preloader_progress_bar_color"].";
				}
				.loading-page{
					background:".$smof_theme_data["preloader_bg_color"].";
				}
				.pace .pace-progress:after{
					color:".$smof_theme_data["preloader_text_color"].";
				}";
			}
		}
		
	
		
		
		if(isset($smof_theme_data["borderradius"]) && $smof_theme_data['borderradius'] == 0){
			
			$custom_css.="
			.parallax-portfolio .bx-viewport, 
			.blogpost,
			.post_format, 
			.overlay span, 
			.parallax-page .member, 
			.blog-post, 
			.sidebar-module, 
			.widget,
			.bx-viewport .recent-item-list li,
			.widget  tfoot td a,
			.comment-meta,
			#comments,
			.btn-group-expand a,
			.portfolio_item,
			.hentry,
			#commentform #author, 
			#commentform #email, 
			#commentform #url, 
			#commentform textarea,
			.search-form .search-field,
			.content-box,
			.btn-sm, 
			.btn-group-sm > .btn, 
			.btn,
			.pagination > li:first-child > a, 
			.pagination > li:first-child > span,
			.pagination,
			#portfolio-cat-filter,
			.nano > .nano-pane > .nano-slider{
				border-radius:0 !important;
			}";
			
		}
		
		if(isset($smof_theme_data["contentbox_bg"]) && !empty($smof_theme_data["contentbox_bg"])){
			$custom_css.="
			.content-box, 
			.parallax-portfolio .slider li,
			.content-box .entry-header, 
			.content-box .team-content, 
			.content-box .team-content h3, 
			.content-box .team-content p, 
			.content-box .team .member, 
			.content-box .blogpost{
				background:".$smof_theme_data["contentbox_bg"].";
			}";
		}
		
		
		if(isset($smof_theme_data["contentbox_text"]) && !empty($smof_theme_data["contentbox_text"])){
			$custom_css.="
			.content-box p, 
			.content-box{
				color:".$smof_theme_data["contentbox_text"].";
			}";
		}
				
		
		if(isset($smof_theme_data["contentbox_pagetitle"]) && !empty($smof_theme_data["contentbox_pagetitle"])){
			$custom_css.="
			.content-box .page-title h1{
				color:".$smof_theme_data["contentbox_pagetitle"].";
			}";
		}
		
		if(isset($smof_theme_data["contentbox_subtitle"]) && !empty($smof_theme_data["contentbox_subtitle"])){
			$custom_css.="
			.content-box .page-title h4{
				color:".$smof_theme_data["contentbox_subtitle"].";
			}";
		}	
					
		if(isset($smof_theme_data["scrollbar_contentbox_color"]) && !empty($smof_theme_data["scrollbar_contentbox_color"])){
			$custom_css.="
			.content-box .nano.has-scrollbar .nano-slider{
				background:".$smof_theme_data["scrollbar_contentbox_color"].";
			}
			";
		}
		
		if(isset($smof_theme_data["scrollbar_color"]) && !empty($smof_theme_data["scrollbar_color"])){
			$custom_css.="
			.nano.has-scrollbar .nano-slider{
				background:".$smof_theme_data["scrollbar_color"].";
			}";		
		}
		

		
		if(isset($smof_theme_data["parallax_width"]) && !empty($smof_theme_data["parallax_width"])){
			$custom_css.=".parallax-page{max-width:".$smof_theme_data["parallax_width"]."px;";
				if(isset($smof_theme_data['responsive']) && $smof_theme_data['responsive'] == 0){
					$custom_css.="width:{$smof_theme_data["parallax_width"]}px;";
				}
			$custom_css.= "}";				
		}
		
		if($smof_theme_data["custom_css"]){
			$custom_css.= $smof_theme_data["custom_css"];
		}
		
		wp_add_inline_style('chosen', $custom_css);
		
		// IE Specific styles.
		wp_enqueue_style( 'horizillax-ie', get_template_directory_uri() . '/css/ie.css', array( 'horizillax-style' ), '2013-07-18' );
		wp_style_add_data( 'horizillax-ie', 'conditional', 'lt IE 9' );
	}
	add_action( 'wp_enqueue_scripts', 'horizillax_scripts_styles' );
	
	
/**
 * Header Scripts
 * ----------------------------------------------------------------------------
 */ 
	function horizillax_header_scripts() {
		$smof_theme_data = of_get_options();
		
    	if(!wp_script_is('header_script', 'done')) {
        	
        	//Load only if Parallax Template is set
    		if ( is_page_template('template-parallax.php') ){
       			echo '<script>';
				//$smof_theme_data = of_get_options();
				$count_pages = wp_count_posts('page');
				$published_pages = $count_pages->publish;
				
				if(isset($smof_theme_data['responsive']) && $smof_theme_data['responsive'] == 1){
					echo 'var responsive = 1;';
				}
				else{
					echo 'var responsive = 0;';
				}				
				if(isset($smof_theme_data['parallax_speed'])&& ($smof_theme_data['parallax_speed'])){ 
					$parallax_speed = $smof_theme_data['parallax_speed'];
				}else{
					$parallax_speed = 2700;
				}
				
				echo 'var transitionSpeed = '.$parallax_speed.';var items =[];';
				//print positions    
				if (! empty($smof_theme_data['pingu_slider'])){ 
					$slides = $smof_theme_data['pingu_slider']; //get the slides array 
					$i=0;
					
					foreach ($slides as $slide) {
					
						//setting all variables if parallax image url exists
						if(isset($slide['url']) && ($slide['url'])){
						
						if (strpos($slide['url'],'[theme-link]') !== false) {
						$slideurl = str_replace('[theme-link]', get_stylesheet_directory_uri(), $slide['url']);
						}
						else{
						$slideurl = $slide['url'];
						}
							if(isset($slide['stackorder']) && ($slide['stackorder'])){ 
								$stackorder = $slide['stackorder'];
							}
							else{
								$stackorder = 0;
							}
				
							if(isset($slide['title']) && ($slide['title'])){ 
								$elemtitle = strtolower($slide['title']);
								$elemtitle= preg_replace("/[^0-9a-zA-Z ]/m", "", $elemtitle);
								$elemtitle = preg_replace("/ /", "-", $elemtitle);
							}
							else{
								$elemtitle = 'page';
							}
								
							if(isset($slide['mousemovespeed']) && ($slide['mousemovespeed'])){ 
								$mousemovespeed = $slide['mousemovespeed'];
							}
							else{
								$mousemovespeed = 0;
							}				
				
							if (isset($slide['vertical_position']) && ($slide['vertical_position']=='top')){
								$vert_position ='tPos';
							}
							else{
								$vert_position ='bPos';
							}
				
							if(isset($slide['vertical_offset']) && ($slide['vertical_offset'])){
								$vertical_offset = $slide['vertical_offset'];
							}
							else{
								$vertical_offset =0;
							}
			
							$positions ="";
							$visibility ="";
							
							for($n=1; $n<=$published_pages; $n++){
							
								if($slide['pos'.$n]){
									$positions.='"'.$slide['pos'.$n].'%"';
								}
								else{
									$positions.='"0%"';
								}
								
								$visibility.='"true"';
								
								if($n!=$published_pages){
 									$positions.=',';
 									$visibility.=',';
 								}
							}//end for n, n++
				
							echo'var item = {};
							item.name = "#'.$elemtitle.$i.'";
							item.stackOrder = '.$stackorder.';
							item.content = "image";
							item.image = "'.$slideurl.'";
							item.sizes = {w:"'.$slide['slide_width'].'",h:"'.$slide['slide_height'].'"};
							item.screenPos = ['.$positions.'];
							item.visibility = ['.$visibility.'];';
							if(isset($smof_theme_data["parallax_mousemove"]) && $smof_theme_data['parallax_mousemove'] == 1){
							echo'item.parallaxScene = true;';
							}else{
							echo'item.parallaxScene = false;';
							}
							echo'item.'.$vert_position.' = '.$vertical_offset.';';
							echo 'item.mouseSpeed = '.$mousemovespeed.';';
							echo'items.push(item);';
					
						}//end if $slide['url'] exists
			
						$i++;
				
					} //end foreach slide
			
				} //end if !empty slider
		
				wp_reset_query();  // Restore global post data

				$i=0;
				$pages =  get_pages('sort_column=menu_order&post_type=page&post_status=publish');
				$total_number_pages = wp_count_posts("page")->publish;
				echo 'var total_num_pages ='.$total_number_pages.';';
				

				foreach ( $pages as $page ) {
				
					if(get_field("top_offset", $page->ID)){
						$top_offset=get_field("top_offset", $page->ID);
					}
					else{
						$top_offset=0;
					}
					setup_postdata( $page );
					$new_title = str_replace( " ", "_", strtolower( $page->post_title ) ).$i;
					$positions ="";
					$visibility="";
 					for($n=0; $n<$published_pages; $n++){
  						$percentage = (($i*100))-(($n*100));
						if ($i==$n){
 							$positions.= '"center"';
 						}
 						else if ($i>$n){
 							$positions.= '"'.($percentage+50).'%"';
 						}
 						else{
 							$positions.= '"'.($percentage-50).'%"';
 						}
 						$visibility.='"true"';
 						if($n!==($published_pages-1)){
 							$positions.= ',';
 							$visibility.=',';
 						}
					}//end for n, n++
					echo'
					var item = {};
					item.name = "#'.$new_title.'";
					item.stackOrder = 99;
					item.content = "html";
					item.screenPos = ['.$positions.'];
					item.offsetV = '.$top_offset.';
					item.visibility = ['.$visibility.'];
					items.push(item);
					';
  					$i++;
				}//End for each page
				wp_reset_postdata();
  
				echo '
				var HORILLAX = {};';
				
				//If page URL has numeric variables	
				if(isset($_GET["page"]) && ctype_digit($_GET["page"]) && $_GET["page"]<=$published_pages){
					echo 'var pagemove = '.$_GET["page"];
				}				
				else{
					echo 'var pagemove = 0;';
				}


				
				echo'
				jQuery(document).ready(function($) {
				$(".nano").nanoScroller({ alwaysVisible: true });
				this.ANIM = new HORILLAX.ANIM();
				HORILLAX.ANIM.instance.move(pagemove);

				});
				
				</script>';

			} //end is_page_template
			
			else{ //if NOT parallax template
			echo'
				<script>
				jQuery(document).ready(function($) {
					$("#parallax-innerpage-menu .menu-item a").attr("href", function(i) {
					return "'.home_url( "/" ).'?page="+(i);
					});
				});
				</script>
				';
			
			}

 			global $wp_scripts;
        	$wp_scripts->done[] = 'header_script';
		}//End if !wp_script_is
		
	}//End function horizillax_header_scripts

	add_action('wp_head', 'horizillax_header_scripts');

/**
 * Footer Scripts
 * ----------------------------------------------------------------------------
 */ 
	function horizillax_footer_scripts() {
		$smof_theme_data = of_get_options();
		
		if(isset($smof_theme_data['google_analytics'])){
			$google_analytics = $smof_theme_data['google_analytics'];
		}	
		echo'<script type="text/javascript">'.$google_analytics.'</script>';
	}
	
	add_action( 'wp_footer', 'horizillax_footer_scripts' );
	

/**
 * Nicely formatted wp_title in head of document, based on current view.
 * ----------------------------------------------------------------------------
 */  
 
	function horizillax_wp_title( $title, $sep ) {
		global $paged, $page;
		if ( is_feed() )
			return $title;
			
		if(!is_archive())
		$title .= get_bloginfo( 'name' );
 		$site_description = get_bloginfo( 'description', 'display' );
 		
		if ( $site_description && ( is_home() || is_front_page() ) )
			$title = "$title $sep $site_description";
			
		if ( $paged >= 2 || $page >= 2 )
			$title = "$title $sep " . sprintf( __( 'Page %s', 'productivethemes' ), max( $paged, $page ) );
		return $title;
	}
	add_filter( 'wp_title', 'horizillax_wp_title', 10, 2 );


/**
 * Displays navigation to next/previous post when applicable.
 * ----------------------------------------------------------------------------
 */  
 
 
 	add_filter( 'previous_image_link', 'horizillax_prev_next_buttonclass' );
	add_filter( 'next_image_link',     'horizillax_prev_next_buttonclass' );
	
	function horizillax_prev_next_buttonclass( $link )
	{
    $class = 'next_image_link' === current_filter() ? 'bx-next btn btn-default' : 'bx-prev btn btn-default';
    return str_replace( '<a ', "<a class='$class'", $link );
	}
 	

	if ( ! function_exists( 'horizillax_post_nav' ) ) :

		// Add btn class to next/previous	
		add_filter('next_post_link', 'horizillax_prev_next_post_buttonclass');
		add_filter('previous_post_link', 'horizillax_prev_next_post_buttonclass');
		function horizillax_prev_next_post_buttonclass($output) {
    		$code = 'class="btn btn-default"';
   			return str_replace('<a href=', '<a '.$code.' href=', $output);
		}
	
		//function to show next and previous links
		function horizillax_post_nav() {
		
			global $post;

			$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
			$next = get_adjacent_post( false, '', false );
				if ( ! $next && ! $previous )
				return;
				?>
			<div class="btn-group">
				<?php previous_post_link( '%link', _x( ' <i class="fa fa-chevron-left"></i>', 'Previous', 'productivethemes' ) ); 

				
				if($post->post_type == "post"){
					echo '<a class="btn btn-default link" data-original-title="SHOW ALL" href="';
					if( get_option( 'show_on_front' ) == 'page' ){ 
						echo get_permalink( get_option( 'page_for_posts' ) );
					}
					else { 
						echo home_url();
					}
					echo '"><i class="fa fa-bars"></i></a>';
				}
				else{
					$portfolio_default_link = get_portfolio_list_link(get_the_ID()); 
					if(get_field("backto_all")){
						$portfolio_showall_link = get_permalink(get_field("backto_all"));
					}
					else{
						$portfolio_showall_link = $portfolio_default_link;
					}
					
					echo '<a class="btn btn-default link" data-original-title="SHOW ALL" href="'.$portfolio_showall_link.'"><i class="fa fa-bars"></i></a>';
				}
				?>
			<?php next_post_link( '%link', _x( ' <i class="fa fa-chevron-right"></i>', 'Next', 'productivethemes' ) ); ?>
			</div>
			<?php
		}
		
	endif;

/**
 * Returns the URL from the post.
 * ----------------------------------------------------------------------------
 */  
	function horizillax_get_link_url() {
		$content = get_the_content();
		$has_url = get_url_in_content( $content );
		return ( $has_url ) ? $has_url : apply_filters( 'the_permalink', get_permalink() );
	}

/**
 * Adds body classes
 * ----------------------------------------------------------------------------
 */  
	function horizillax_body_class( $classes ) {
		if ( ! is_multi_author() )
			$classes[] = 'single-author';
	
		if ( is_active_sidebar( 'sidebar-2' ) && ! is_attachment() && ! is_404() )
			$classes[] = 'sidebar';
	
		if ( ! get_option( 'show_avatars' ) )
			$classes[] = 'no-avatars';
	
		return $classes;
	}
	add_filter( 'body_class', 'horizillax_body_class' );

/**
 * Sets up the content width value based on the theme's design.
 * @see productivethemes_content_width() for template-specific adjustments.
 */
 
	if ( ! isset( $content_width ) )
		$content_width = 650;

/**
 * Add postMessage support for site title and description for the Customizer.
 * ----------------------------------------------------------------------------
 */   
	function horizillax_customize_register( $wp_customize ) {
		$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
		$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	}
	add_action( 'customize_register', 'horizillax_customize_register' );

	/*Get Video Embedd*/
	function horizillax_video_embed($vurl,$height = "100%",$width="100%"){
		$image_url = parse_url($vurl);
		// Test if the link is for youtube
		$youtube_autoplay = get_field("video_autoplay","options") == "Enabled" ? "&autoplay=1" : "";
		if($image_url['host'] == 'www.youtube.com' || $image_url['host'] == 'youtube.com'){
			$array = explode("&", $image_url['query']);
			return '<iframe class="youtube" src="http://www.youtube.com/embed/' . substr($array[0], 2) . '?wmode=transparent&enablejsapi=1'.$youtube_autoplay.'" width="'.$width.'" height="'.$height.'" frameborder="0" allowfullscreen></iframe>'; // Returns the youtube iframe embed code
			// Test if the link is for the shortened youtube share link
		} else if($image_url['host'] == 'www.youtu.be' || $image_url['host'] == 'youtu.be'){
			$array = ltrim($image_url['path'],'/');
			return '<iframe class="youtube" src="http://www.youtube.com/embed/' . $array . '?wmode=transparent&enablejsapi=1'.$youtube_autoplay.'" width="'.$width.'" height="'.$height.'" frameborder="0" allowfullscreen></iframe>'; // Returns the youtube iframe embed code
			// Test if the link is for vimeo
		} else if($image_url['host'] == 'www.vimeo.com' || $image_url['host'] == 'vimeo.com'){
			$hash = substr($image_url['path'], 1);
			return '<iframe class="vimeo" src="http://player.vimeo.com/video/' . $hash . '?title=0&byline=0&portrait=0&api=1'.$youtube_autoplay.'" width="'.$width.'" height="'.$height.'" frameborder="0" webkitAllowFullScreen allowfullscreen></iframe>'; // Returns the vimeo iframe embed code
		}
	}


/**
 * Binds JavaScript handlers to make Customizer preview reload changes asynchronously.
 * ----------------------------------------------------------------------------
 */   
 
	function horizillax_customize_preview_js() {
		wp_enqueue_script( 'productivethemes-customizer', get_template_directory_uri() . '/js/theme-customizer.js', array( 'customize-preview' ), '20130226', true );
	}
	add_action( 'customize_preview_init', 'horizillax_customize_preview_js' );

/**
 * Custom Pagination for bootstrap
 * ----------------------------------------------------------------------------
 */  
	
function horizillax_pagination($before = '', $after = '') {
	global $wpdb, $wp_query;
	$request = $wp_query->request;
	$posts_per_page = intval(get_query_var('posts_per_page'));
	$paged = intval(get_query_var('paged'));
	$numposts = $wp_query->found_posts;
	$max_page = $wp_query->max_num_pages;
	if ( $numposts <= $posts_per_page ) { return; }
	if(empty($paged) || $paged == 0) {
		$paged = 1;
	}
	$pages_to_show = 7;
	$pages_to_show_minus_1 = $pages_to_show-1;
	$half_page_start = floor($pages_to_show_minus_1/2);
	$half_page_end = ceil($pages_to_show_minus_1/2);
	$start_page = $paged - $half_page_start;
	if($start_page <= 0) {
		$start_page = 1;
	}
	$end_page = $paged + $half_page_end;
	if(($end_page - $start_page) != $pages_to_show_minus_1) {
		$end_page = $start_page + $pages_to_show_minus_1;
	}
	if($end_page > $max_page) {
		$start_page = $max_page - $pages_to_show_minus_1;
		$end_page = $max_page;
	}
	if($start_page <= 0) {
		$start_page = 1;
	}
		
	echo $before.'<ul class="pagination">'."";
	if ($paged > 1) {
		$first_page_text = "«";
		echo '<li class="prev"><a href="'.get_pagenum_link().'" title="First">'.$first_page_text.'</a></li>';
	}
		
	$prevposts = get_previous_posts_link('‹');
	if($prevposts) { echo '<li>' . $prevposts  . '</li>'; }
	else { echo '<li class="disabled"><a href="#">‹</a></li>'; }
	
	for($i = $start_page; $i  <= $end_page; $i++) {
		if($i == $paged) {
			echo '<li class="active"><a href="#">'.$i.'</a></li>';
		} else {
			echo '<li><a href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
		}
	}
	echo '<li class="">';
	next_posts_link('›');
	echo '</li>';
	if ($end_page < $max_page) {
		$last_page_text = "»";
		echo '<li class="next"><a href="'.get_pagenum_link($max_page).'" title="Last">'.$last_page_text.'</a></li>';
	}
	echo '</ul>'.$after."";
}	