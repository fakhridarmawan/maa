<?php get_header(); ?>

	
		<?php if( get_post_type() == 'post' ):?>
			<div class="container">
				<div class="row">
					<?php if ( have_posts() ) : ?>
						<div class="page-title">
						<?php
								if ( is_day() ) :
									printf( __( '<h4>Daily Archives</h4> <h1>%s</h1>', 'productivethemes' ), get_the_date() );
								elseif ( is_month() ) :
									printf( __( '<h4>Monthly Archives</h4> <h1>%s</h1>', 'productivethemes' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'productivethemes' ) ) );
								elseif ( is_year() ) :
									printf( __( '<h4>Yearly Archives</h4> <h1>%s</h1>', 'productivethemes' ), get_the_date( _x( 'Y', 'yearly archives date format', 'productivethemes' ) ) );
								else :
									_e( '<h1>Archives </h1>', 'productivethemes' );
								endif;?>
                            
							
						</div><!-- .main-header -->
						
			<div id="primary" class="col-sm-9 <?php if(isset($data['responsive']) && !$data['responsive'] == 1){ echo 'col-xs-9 col-md-9';}?> blog-main">
				<div id="content" class="site-content" role="main">
						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'content'); ?>
						<?php endwhile; ?>
				</div><!-- #content -->
				<div class="btn-group-wrap">
				<?php if (function_exists("horizillax_pagination")) { horizillax_pagination();} ?>
				</div>	
			</div><!-- #primary -->
					
            
					<?php else: ?>
			<div id="primary" class="col-sm-9 <?php if(isset($data['responsive']) && !$data['responsive'] == 1){ echo 'col-xs-9 col-md-9';}?> blog-main">
				<div id="content" class="site-content" role="main">						
				<?php get_template_part( 'content', 'none' ); ?>
					<?php endif; ?>
			<?php get_sidebar();?>
			</div>	<!--.row -->	
		</div> <!--.container -->	
		<?php else: ?>
		
		
		
			<div class="container">
				<div class="row">		
			<div class="page-title">
				<h1 class="main-title">
               		<?php wp_title("",true); ?>
                </h1>
            </div><!-- .main-header -->
            
            
            
			<?php while ( have_posts() ) : the_post(); 
				$terms = wp_get_post_terms( $post->ID,"portfolio_category" );
                $terms_html_array = array();
                $terms_id_array = array();
                $term_classes = "";
                foreach($terms as $t){
                    $term_name = $t->name;
                    $term_link = get_term_link($t->slug,$t->taxonomy);
                    array_push($terms_html_array,"<a href='{$term_link}'>{$term_name}</a>");
                    array_push($terms_id_array,$t->slug);
                    $term_classes .= "pt_".$t->slug." ";
                }
                $terms_html_array = implode(" / ",$terms_html_array);
			?>
				<div class="col-sm-4 <?php if(isset($data['responsive']) && !$data['responsive'] == 1){ echo 'col-xs-4 col-md-4';}else{echo'col-xs-6';}?>">
					<div class="portfolio_item">
						<div class="overlay">
							<?php 
								if(has_post_thumbnail()) {
									the_post_thumbnail('portfolio_medium', array('class'	=> "img-responsive"));
									$large_image    = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
								} else {
									echo '<img class="img-responsive" src="'.get_bloginfo("template_url").'/images/default-featured-image.png" width="200" height="200" />';
								}
							?>
							<span class="icon-align two-icons">
								<a href="<?php echo $large_image[0];?>" title="<?php the_title(); ?>" class="icon magnify"><i class="fa fa-search"></i></a>
								<a href="<?php the_permalink(); ?>" class="icon"><i class="fa fa-link"></i></a>
							</span><!-- .portfolio_meta -->
						</div><!-- .overlay -->
						<div class="entry-header small-title">
							<div class="equalheight">
							<h1 class="blog-post-title">
								<a href="<?php the_permalink();?>"><?php the_title(); ?></a>
                   			</h1><!-- .portfolio_title -->
                   			<?php if($terms_html_array): ?>
                   				<div class="cat-tag">
								<div class="categories_list">
									<i class="fa fa-folder-open"></i>
                       	    			<?php echo $terms_html_array; ?>
								</div>
								</div>
							<?php endif; ?>	
						</div>				
						</div>
					</div><!-- .portfolio_item -->
				</div><!-- .cols -->			
			<?php endwhile; ?>
			</div>	<!--.row -->	
			<div class="btn-group-wrap">
							<?php if (function_exists("horizillax_pagination")) { horizillax_pagination();} ?>
			</div>				

		</div> <!--.container -->				
			
			
		<?php endif; ?>
		
		
		
<?php get_footer(); ?>