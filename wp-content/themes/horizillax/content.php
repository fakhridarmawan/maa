<?php
/**
 * The default template for displaying content.
 */
?>

<?php if ( 'post' == get_post_type() ){?>
<article id="post-<?php the_ID(); ?>" <?php post_class('blog-post'); ?>>
<div class="post_format fa"></div>
<?php } else{ ?>
<article id="post-<?php the_ID(); ?>" <?php post_class('blog-post not-post'); ?>>
<?php } ?>
	<header class="entry-header">
			<?php if ( is_single() ) : ?>
				<h1 class="blog-post-title"><?php the_title(); ?></h1>
			<?php else : ?>
				<h1 class="blog-post-title">
					<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
				</h1>
			<?php endif; // is_single() ?>
					<div class="meta-details">
					<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ));?>"><i class="fa-user fa"></i> <?php  echo get_the_author();?></a>
					<a href="<?php echo get_permalink();?>"><i class="fa-clock-o fa"></i> <?php echo get_the_date();?></a>
					<a href="<?php echo get_permalink().'#comments';?>"><i class="fa-comment fa"></i> <?php echo get_comments_number();?></a>
					</div>
			
			<?php edit_post_link( __( 'Edit', 'productivethemes' ), '<span class="edit-link">', '</span>' ); ?>
	</header><!-- .entry-header -->
	
    			<div class="post_format fa"></div>
				<div class="entry-meta">
					<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
						<?php the_post_thumbnail('blog_medium'); ?>
						</div>
					<?php endif; ?>
				</div><!-- .entry-meta -->
	
	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
		<div class="entry-content">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
	<?php else : ?>

      		
		<div class="cat-tag">
			<div class="categories_list"><i class="fa fa-folder-open"></i><?php echo get_the_category_list( ' / ');?></div>
			<div class="tags_list"><?php echo get_the_tag_list('<i class="fa fa-tag"></i>',' / ','');?></div>
		</div>
			
		<div class="entry-content">
			<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'productivethemes' ) ); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'productivethemes' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
    	</div><!-- .entry-content -->
	<?php endif; ?>
	<footer class="entry-meta">
		<?php if ( is_single() && get_the_author_meta( 'description' ) && is_multi_author() ) : ?>
			<?php get_template_part( 'author-bio' ); ?>
		<?php endif; ?>
	</footer><!-- .entry-meta -->
	
	<?php if ( is_single() ): ?>
	<div class="btn-group-wrap"><?php horizillax_post_nav(); ?></div>
	<?php endif; ?>
	
</article><!-- #post -->
