<?php

function displayPollResults( $vars){
    
    $poll_answers = $vars['poll_answers'];
    $total = $vars['total_votes'];
    $option_value = $vars['option_value'];
    $poll = $vars['poll'];
    $answer_type = $poll->answer_type;
    $total_votes = 0;  
    $poll_id = $vars['poll']->id;
    if(!empty($option_value['bar_height'])) 
        $bar_height = $option_value['bar_height'];
    else $bar_height = "10";
    if(!empty($option_value['bar_color'])) 
        $bar_color = $option_value['bar_color'];
    else 
        $bar_color = "CCC";
		
		
	$theme = get_option("lighty_option");
	
	if($poll_id == $theme['other_vote1']){
		$page = explode(',',$theme['page_vote1']);
	}else{
		$page = explode(',',$theme['page_vote2']);
	}
	
    ?>
	<script>
		jQuery(document).ready(function(){
			jQuery(".inline").colorbox({inline:true, width:"50%"});
		})
	</script>
    <div class="show-results<?php echo $poll_id;?>">
    <?php
        if($answer_type == "multiple") print "<div class='total-voters'><b>".__("Total voters", "cardozapolldomain").": </b>".$total."</div>";

        foreach($poll_answers as $answer){
            $total_votes = $total_votes + $answer->votes;
        }
        print "<div class='total-votes'><b>".__("Total votes", "cardozapolldomain").": </b>".$total_votes."</div>";
        $answer_count = 0;
        $total_answer_count = sizeof($poll_answers);
        $total_width = 0;
		$i=0;
		$total_graph = "";
        foreach($poll_answers as $answer){
			if(($i%2) == 0){
				$dss = ' first ';
			}else{
				$dss = '';
			}
			$i++;
            $answer_count++;
            $votes = $answer->votes;
            if($total_votes!=0) $width = ($votes/$total_votes)*100;
            else $width = 0;
            if($answer_count == $total_answer_count && $total_votes>0) $width = 100 - $total_width;
            else $total_width = $total_width + round($width);
            if($poll->poll_type == 'image_poll'){
				echo "<a class='inline' href='.a-".$i."'>";
                print "<div class='result-answer span6 $dss'>
							<div class='vote-image img-polaroid'>
								<div style='width:100%;height:100%;overflow:hidden'>
									<img src='".$answer->answer."' width='100' alt='".$answer->answer."'/>
								</div>
							</div> 
							(".$answer->votes.__(" votes", "cardozapolldomain").", ".round($width)."%)
						</div>";
				echo "</a>";
				echo "<div style='display:none'>
							<div class='a-".$i."' style='padding:10px; background:#fff;'>";
								
								$post_7 = get_post($page[$i], ARRAY_A);
								$title = $post_7['post_title'];
								echo "<h3>".$title."</h3>";
								echo "<p>".$post_7['post_content']."</p>";
				echo "		</div>
						</div>";
				$nama = explode('/',$answer->answer);
				$nama1 = explode('.',$nama[(count($nama)-1)]);		
						
				$total_graph .= "['".$nama1[0]."',   ".round($width)."],";
            }else{
                print "<div class='result-answer'>".$answer->answer." (".$answer->votes.__(" votes", "cardozapolldomain").", ".round($width)."%)</div>";
			}
        }
		
    ?>
	<div class='clearfix'></div>
	<br />
	<script type="text/javascript">
			jQuery(function () {
				jQuery('#graph').highcharts({
					chart: {
						plotBackgroundColor: null,
						plotBorderWidth: null,
						plotShadow: false,
						backgroundColor:'rgba(255, 255, 255, 0.1)'
					},
					title: {
						text: ''
					},
					tooltip: {
						pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: true,
								format: '<b>{point.name}</b>: {point.percentage:.1f} %',
								style: {
									color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
								}
							}
						}
					},
					series: [{
						type: 'pie',
						name: 'Vote',
						data: [
							<?php echo $total_graph; ?>
						]
					}]
				});
			});
		</script>
		<div id='graph'></div>
    </div>
	<?php
}

function showPollForm($vars){
    
    $poll_answers = $vars['poll_answers'];
    $total = $vars['total_votes'];
    $option_value = $vars['option_value'];
    $poll = $vars['poll'];
    
    $option = 1;
    $exp_time = $vars['exp_time'];
    if(empty($poll->no_of_answers)) $poll->no_of_answers = 100;
	
	
    ?>
    <div id="show-form<?php echo $poll->id;?>" class="show-form<?php echo $poll->id;?>">
        <?php
        print '<table>';
        foreach($poll_answers as $answer){
            if($poll->answer_type == "one"){
                    print '<tr><td width="20" valign="middle"><input type="radio" id="option'.$poll->id.'" name="'.$poll->id.'" value="'.$answer->id.'" /></td>';
                    if($poll->poll_type == 'image_poll') {
                        print '<td><label for="option'.$option.'"><img src="'.$answer->answer.'" width="100" alt="'.$answer->answer.'" /></label><br />';
						$nama = explode('/',$answer->answer);
					$nama1 = explode('.',$nama[(count($nama)-1)]);
					echo " - ".$nama1[0];
					echo '</td>';
                    }
                    else {
                        print '<td align="left">'.$answer->answer.'</td>';
                    }
                    print '</tr>';
            }
            if($poll->answer_type == "multiple"){
                print '<tr><td width="20" valign="middle"><input type="checkbox" id="option'.$option.'" name="option'.$option.'" value="'.$answer->id.'" /></td>';
                if($poll->poll_type == 'image_poll') {
                        print '<td><label for="option'.$option.'"><img src="'.$answer->answer.'" width="100" alt="'.$answer->answer.'"/></label><br />';
					$nama = explode('/',$answer->answer);
					$nama1 = explode('.',$nama[(count($nama)-1)]);
					echo " - ".$nama1[0];
					echo '</td>';
                    }
                    else {
                        print '<td align="left">'.$answer->answer.'</td>';
                    }
                print '</tr>';
            }
            $option++;
        }
        print '</table>';
        ?>
        <input type="hidden" value="<?php print $poll->id;?>" name="poll_id" />
        <input type="hidden" value="<?php print $exp_time;?>" name="expiry" />
        <input type="hidden" value="<?php print $poll->answer_type;?>" name="answertype"/>
        <input type="hidden" value="<?php print $poll->no_of_answers;?>" name="max_no_of_answers" />
        <input type="hidden" value="submit_vote" name="action"/>
        <center><input class="poll-wh-style" type="button" value="<?php print __('Vote', 'cardozapolldomain');?>" onclick="javascript:vote_poll(<?php print $poll->id;?>,'<?php print $poll->answer_type;?>',<?php print $poll->no_of_answers;?>)" /></center>                
    </div>
    <?php
}

function showPollFormSC($vars){ 
    
    $poll_answers = $vars['poll_answers'];
    $total = $vars['total_votes'];
    $option_value = $vars['option_value'];
    $poll = $vars['poll'];
    $option = 1;
    $exp_time = $vars['exp_time'];
    if(empty($poll->no_of_answers)) $poll->no_of_answers = 100;
    ?>
    <div id="show-form<?php echo $poll->id;?>" class="show-form<?php echo $poll->id;?>">
		<script>
			jQuery(document).ready(function(){
				jQuery(".inline").colorbox({inline:true, width:"50%"});
			})
			
		</script>
		<div class="total-votes">&nbsp;</div>
        <?php
		$i=-1;
		$theme = get_option("lighty_option");
		if($poll->id == $theme['other_vote1']){
			$page = explode(',',$theme['page_vote1']);
		}else{
			$page = explode(',',$theme['page_vote2']);
		}
        foreach($poll_answers as $answer){
			$i++;
            if($poll->answer_type == "one"){
                    if($poll->poll_type == 'image_poll') {
						if(($i %2) == 0){
							echo "";
						}else{
							echo "";
						}
						$nama = explode('/',$answer->answer);
						$nama1 = explode('.',$nama[(count($nama)-1)]);
						$nama = str_replace('-',' ',$nama1[0]);
						echo "<div class='span6 first'>
								<a class='inline' href='.a".$poll->id.'-'.$i."'>
								<div class='vote-image img-polaroid'>
									<div style='width:100%;height:100%;overflow:hidden'>
									<img src='".$answer->answer."'>
									</div>
								</div>
								</a>
								<h5><input type='radio' id='".$poll->id.'-'.$i."' name='".$poll->id."' value='".$answer->id."' style='margin-bottom:5px;'/> $nama</h5>
								<div style='display:none'>
									<div class='a".$poll->id.'-'.$i."' style='padding:10px; background:#fff;'>";
										
										$post_7 = get_post($page[$i], ARRAY_A);
										$title = $post_7['post_title'];
										echo "<h3>".$title."</h3>";
										echo "<p>".$post_7['post_content']."</p>";
						echo "		</div>
								</div>
							</div>";
                    }
                    else {
                        print '<td><input type="radio" id="'.$poll->id.'-'.$i.'" name="'.$poll->id.'" value="'.$answer->id.'" /> '.$answer->answer.'</td>';
                    }
                    print '</tr>';
            }
            if($poll->answer_type == "multiple"){
                print '<tr><td valign="middle"><input type="checkbox" name="option'.$option.'" value="'.$answer->id.'" /></td>';
                if($poll->poll_type == 'image_poll') {
						if(($i %2) == 0){
							echo "";
						}else{
							echo "";
						}
						$nama = explode('/',$answer->answer);
						$nama1 = explode('.',$nama[(count($nama)-1)]);
						$nama = str_replace('-',' ',$nama1[0]);
						echo "<div class='span6 first'>
								<a class='inline' href='.a".$poll->id.'-'.$i."'>
								<div class='vote-image img-polaroid'>
									<div style='width:100%;height:100%;overflow:hidden'>
									<img src='".$answer->answer."'>
									</div>
								</div>
								</a>
								<h5><input type='radio' id='".$poll->id.'-'.$i."' name='".$poll->id."' value='".$answer->id."' style='margin-bottom:5px;'/> $nama</h5>
								<div style='display:none'>
									<div class='a".$poll->id.'-'.$i."' style='padding:10px; background:#fff;'>";
										
										$post_7 = get_post($page[$i], ARRAY_A);
										$title = $post_7['post_title'];
										echo "<h3>".$title."</h3>";
										echo "<p>".$post_7['post_content']."</p>";
						echo "		</div>
								</div>
							</div>";
                        //print '<td><img src="'.$answer->answer.'" width="100"  alt="'.$answer->answer.'" />bb</td>';
                    }
                    else {
                        print '<td>'.$answer->answer.'</td>';
                    }
                print '</tr>';
            }
            $option++;
        }
        ?>
		<div class='clearfix'></div>
        <input type="hidden" value="<?php print $poll->id;?>" name="poll_id" />
        <input type="hidden" value="<?php print $exp_time;?>" name="expiry" />
        <input type="hidden" value="<?php print $poll->answer_type;?>" name="answertype"/>
        <input type="hidden" value="<?php print $poll->no_of_answers;?>" name="max_no_of_answers" />
        <input type="hidden" value="submit_vote" name="action"/>
        <center>
			<input type="button" class="poll-wh-style btn flat" style='padding:8px 30px;float:left; margin-right:10px;background-color:#333333;color:#fff' value="<?php print __('Vote', 'cardozapolldomain');?>" onclick="javascript:vote_poll_sc(<?php print $poll->id;?>,'<?php print $poll->answer_type;?>',<?php print $poll->no_of_answers;?>)" />
			</center>                
    </div>
    <?php
}

function previousPollsLink($vars){
    
    $option_value = $vars['option_value'];
    
    if($option_value['archive']=='yes'){
        ?>
        <div style="margin-top:10px;margin-bottom:10px;width:100%; border-bottom: 1px #000 dotted"></div>
        <?php
        $archive_url = $option_value['archive_url'];
        $url = explode('?', $archive_url);
        if(sizeof($url)>1) echo '<a href="'.$option_value['archive_url'].'&poll_archive_page_no=1">'.__('See all polls and results', 'cardozapolldomain').'</a>';
        else echo '<a href="'.$option_value['archive_url'].'?poll_archive_page_no=1">'.__('See all polls and results', 'cardozapolldomain').'</a>';
    }
}
?>
