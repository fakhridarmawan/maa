<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'inspirasimaa');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Pm)}m/^/@+h5,o0+MgYbyTX4t{F<|`4wb?tl?RF#9.FS!*,_AM4~P+3?R(v~l/E}');
define('SECURE_AUTH_KEY',  'ptW#I1fhO7r  B=}fZ6q7H*Qql`^LK?}_aU1|@M,=T>E:+<q,CQv-zwK D|`,(Mz');
define('LOGGED_IN_KEY',    'UyN9<W8=3TrNxU$]A5=Pce-qL-Yq  y35772k|Y(Jb A38R<|,N.n^4DmAG6:>}+');
define('NONCE_KEY',        ')k4k_NnRh(rx((|lH1k)yGi=ep.egnIcT1eaZSTx!KGI&If,vB<=m5H7$L^P-fs`');
define('AUTH_SALT',        'sS;Ny}(MYKsg*LLT?!v`s|O-}ZUZyfp*V6`zm$l?G*&`oYj2 #?q}k7~tKL>r7:8');
define('SECURE_AUTH_SALT', ']C^X:Eu!BYM[hk_Jy_%jr:e0USB!g0}Bv>B+baaX|*,:Vry>go!:^W694|7MN<8I');
define('LOGGED_IN_SALT',   'DDD)|{aSk(!6We9I{BMW`j5 u !I&zvri&ZsK{zzS9+w/?I-FUIrL|},|]M-yW<&');
define('NONCE_SALT',       'a<!OAn@y>sW9mQ/k]Id`+o#Z,VAHh(g@{=phW*[4|zw5Pm{k@6vOg=ZchZ6;S7|2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'maa_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
